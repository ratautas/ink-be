<?php

namespace Api\MailChimper;

/**
 * Class to manage sending newsletters throw Mailchimp & Mandrill
 */
class MailChimper
{
    const SUBJECT_PREFIX_LT = 'Jud(INK) smegenis: ';

    const SUBJECT_PREFIX_EN = 'Outhh(INK): ';

    /**
     * Action type.
     * Can be mc-test|mc-send
     */
    private $action;
    
    private $postid;
    
    private $email;

    /**
     * Global formated post
     */
    private $postFormated = [];

    /**
     * Log file location, relative to wp-content
     */
    private $log_file = "wp-content/mailchimper.log";

    /**
     * Event log status
     */
    private $log_file_is_active = true;

    /**
     * Mailchimps vars
     */
    private $api_key;
    private $data_center;
    private $list_id_lt;
    private $list_id_en;

    /**
     * Mandrill vars
     */
    private $mandrill_key = 'gm6OLXgplLVo9uPgNPBJbA';
    private $template_name_lt = 'PAILS(INK) SMEGENIS';
    private $template_name_en = 'Outhh(INK)'; // set

    /**
     * holds all members
     */
    private $member_lt = [];
    private $member_en = [];

    /**
     * Create a new instance
     */
    public function __construct( $action, $postid, $email )
    {
        $this->action = $action;
        $this->postid = $postid;
        $this->email = trim($email);
        $this->log_file = get_home_path().$this->log_file;

        $this->setMailchimpOptions();
        
    }

    /**
     * Manage only mailchimp @var
     */
    private function setMailchimpOptions()
    {
        $mailchimp_options = get_field('mailchimp', 'option');

        if (strpos($mailchimp_options['key'], '-') === false) {
            $this->setLog( "Neteisingas mailchimp KEY formatas." );
            throw new \Exception("Neteisingas mailchimp KEY formatas.");
        }
        
        list( $api_key, $data_center) = explode('-', $mailchimp_options['key']);
        $this->api_key = $api_key;
        $this->data_center = $data_center;
        $this->list_id_lt = $mailchimp_options['id'];
        $this->list_id_en = $mailchimp_options['id_en'];
    }

    /**
     * Main
     * @return array
     */
    public function run()
    {
        $has_data = $this->getPost();
        $is_sended = $this->getSendStatus();

        if( empty($this->api_key) && empty($this->data_center) && (empty($this->list_id_lt) || empty($this->list_id_en)) )
        {
            $response['status'] = false;
            $response['message'] = 'Nenurodyti mailchimp KEY ir sąrašo ID';
            return $response;
        }
        
        if( $has_data === true && $is_sended === false )
        {
            if( $this->action == 'mc-test' && filter_var($this->email, FILTER_VALIDATE_EMAIL) )
            {
                
                if( !empty($this->mandrill_key) && (!empty($this->template_name_lt) || !empty($this->template_name_en) ) )
                    $template_data = $this->setTemplate( 'lt', true );
                else
                {
                    $response['status'] = false;
                    $response['message'] = 'Nenurodyti Mandrill raktai';
                    $this->setLog( $response['message'] );
                    return $response;
                }
                
                $is_send = $this->sendNewsletters( $template_data );

                if( $is_send === true )
                {
                    $response['status'] = true;
                    $response['message'] = 'Testinis naujienlaiškis išsiųstas sėkmingai!';
                }
                else
                {
                    $response['status'] = false;
                    $response['message'] = 'Nepavyko išsiųsti testinio naujienlaiškio. Bandykite vėliau.';
                    $this->setLog( $response['message'] );
                }
            } 
            elseif( $this->action == 'mc-send' )
            {
                $this->member_lt = $this->getMembers( $this->list_id_lt );
                $this->member_en = $this->getMembers( $this->list_id_en );

                if( !empty($this->mandrill_key) && (!empty($this->template_name_lt) || !empty($this->template_name_en) ) )
                {
                    $template_data_lt = $this->setTemplate( 'lt', false );
                    $template_data_en = $this->setTemplate( 'en', false );
                }
                else
                {
                    $response['status'] = false;
                    $response['message'] = 'Nenurodyti Mandrill raktai';
                    $this->setLog( $response['message'] );
                    return $response;
                }

                $is_send = $this->sendNewsletters( $template_data_lt );
                $is_send_en = $this->sendNewsletters( $template_data_en );

                if( $is_send === true && $is_send_en === true )
                {
                    $response['status'] = true;
                    $response['message'] = 'Naujienlaiškiai išsiųsti sėkmingai! <br/> LT - '.count($this->member_lt).' vnt.; EN - '.count($this->member_en).' vnt.';
                    $this->setSendStatus();
                }
                else
                {
                    // @todo: if will be some problem with EN newsletter - set sending by lang optionaly
                    $response['status'] = false;
                    $response['message'] = 'Nepavyko išsiųsti naujienlaiškių. Bandykite vėliau.';
                    $this->setLog( $response['message'] );
                    
                }
            }
        }
        else
        {
            $response['status'] = false;
            if( $is_sended === true )
            {
                $response['message'] = 'Įrašas jau buvo išsiųstas.';
                $this->setLog( $response['message'] );
            }
            elseif( $has_data === false )
            {
                $response['message'] = 'Nėra įrašo duomenus';
                $this->setLog( $response['message'] );
            }
        }

        return $response;
    }

    /**
     * 
     */
    private function getPost()
    {
        $currentPost = get_post( $this->postid );

        $eng_id = icl_object_id( $this->postid, 'post', false, 'en' );
        $currentPost_en = null;
        if( (int)$eng_id > 0 )
            $currentPost_en = get_post( $eng_id );

        $this->getPostBase( $currentPost, $currentPost_en );
        $this->getPostTexts( $currentPost, $currentPost_en );
        $this->getPostImages();

        if( is_array($this->postFormated) && !empty($this->postFormated) )
            return true;
    }

    /**
     * Post base information
     * @return void
     */
    private function getPostBase( $data, $data_en = null )
    {
        $this->postFormated['name']['lt']	= $data->post_title;
        $this->postFormated['name']['en']	= ( !empty($data_en->post_title) ) ? $data_en->post_title : $data->post_title;
        $this->postFormated['name_subject']['lt'] = get_class($this)::SUBJECT_PREFIX_LT.$data->post_title;
        $this->postFormated['name_subject']['en'] = get_class($this)::SUBJECT_PREFIX_EN.$this->postFormated['name']['en'];
        $this->postFormated['date'] 	= $data->post_date;
        $this->postFormated['author'] = $this->getAuthor();
    }

    
    /**
     * Post text information
     * @return void
     */
    private function getPostTexts( $data, $data_en = null )
    {
        // lt text
        $text01	            = '<p>'.$data->post_content.'<p>';
        $post_additional    = get_field('intro', $this->postid);
        $text01             .= $post_additional['lead'];
        $text01             .= $post_additional['text'];
        $post_sections      = get_field('sections', $this->postid);
        if( is_array($post_sections) && count($post_sections) > 0 )
        {
            foreach( $post_sections as $value )
            {
                if( $value['type'] == 'text' )
                {
                if( $value['title'] )
                    $text01 .= '<p>'.$value['title'].'<p>';
                $text01 .= $value['text'];
                }
            }
        }
        $this->postFormated['text01']['lt'] = preg_replace("/<img[^>]+\>/i", " ", $text01);

        // en text
        $eng_id = icl_object_id( $this->postid, 'post', false, 'en' );
        if( (int)$eng_id > 0 )
        {
            unset( $text01 );
            unset( $post_additional );
            unset( $post_sections );
            $text01	            = '<p>'.$data_en->post_content.'<p>';
            $post_additional    = get_field('intro', $eng_id);
            $text01             .= $post_additional['lead'];
            $text01             .= $post_additional['text'];
            $post_sections      = get_field('sections', $eng_id);
            if( is_array($post_sections) && count($post_sections) > 0 )
            {
                foreach( $post_sections as $value )
                {
                    if( $value['type'] == 'text' )
                    {
                    if( $value['title'] )
                        $text01 .= '<p>'.$value['title'].'<p>';
                    $text01 .= $value['text'];
                    }
                }
            }
            $this->postFormated['text01']['en'] = preg_replace("/<img[^>]+\>/i", " ", $text01); 
        }
        else
            $this->postFormated['text01']['en'] = $this->postFormated['text01']['lt'];
    }

    /**
     * Post images information
     * @return void
     */
    private function getPostImages()
    {
        $mc_image = get_field('mc_image', $this->postid);
        if( $mc_image )
            $image = $mc_image;
        else
            $image = NULL;
        
        $this->postFormated['image'] = $image;

        $mailchimp_options = get_field('mailchimp', 'option');
        $this->postFormated['image_header']['lt'] = $mailchimp_options['mailchimp_template_header_lt'];
        $this->postFormated['image_header']['en'] = $mailchimp_options['mailchimp_template_header_en'];
    }
    
    /**
     * Events logs
     * @param string $message
     * @return void
     */
    private function setLog( $message )
    {
        if( $this->log_file_is_active === true )
        {
            $fp = fopen( $this->log_file, 'a' );
            fwrite( $fp, date("Y-m-d H:i:s") ." ". print_r($message, true)."\n" );
            fclose( $fp );
        }
    }

    /**
     * Check is post was sent
     * @return boolean true if yes
     */
    private function getSendStatus()
    {
        $status = get_field('mailchimp_sended', $this->postid);
        if( is_array($status) && !empty($status[0]) && $status[0] == 'sended' )
            return true;
        else
            return false;
    }

    /**
     * Set post status as sent
     * @return void
     */
    private function setSendStatus()
    {
        $status = get_field('mailchimp_sended', $this->postid);
        update_field( 'mailchimp_sended', array( 0 => 'sended' ), $this->postid );
    }

    /**
     * Get all newsletters members
     * @param int $list_id
     * @return json array
     */
    private function getMembers( $list_id )
    {
        $auth = base64_encode( 'user:'.$this->api_key );
        $headers = array('Accept' => 'application/json',  'Authorization' => 'Basic '.$auth );
        $url = "https://".$this->data_center.".api.mailchimp.com/3.0/lists/".$list_id."/members?count=2000";

        $request = \Requests::request( $url, $headers, null, 'GET', array('timeout'=>5000) );

        if( $request->status_code == 200 && $request->success == 1 )
        {
            $members_all = json_decode( $request->body );
            $recipient_all = array();
            $i = 0;
            if( is_array($members_all->members) )
            {
                foreach( $members_all->members as $value )
                {
                    if( $value->status == "subscribed" ) 
                    {
                        $recipient_all[$i]['email'] = $value->email_address;
                        $recipient_all[$i]['type'] = "bcc";
                        $i++;
                    }
                }
            }
        }

        return $recipient_all;
    }

    /**
     * Format mandrill template main params: header && option
     * @param string $lang prefix
     * @param boolean true if test
     * @return array
     */
    private function setTemplate( $lang = 'lt', $is_test )
    {

        $to = $this->setToParam( $lang, $is_test );

        $headers = array('Accept' => 'application/json' );			
        $html =  preg_replace('!\s+!m', ' ', addslashes( $this->postFormated['text01'][$lang] ));
        $file_cont = file_get_contents( $this->postFormated['image'] );
        $image_prepared = base64_encode( $file_cont );
        
        $file_cont_image_header = file_get_contents( $this->postFormated['image_header'][$lang] );
        $image_prepared_header = base64_encode( $file_cont_image_header );
                                        
        $blog_entry_date = date_i18n( 'Y F j', strtotime($this->postFormated['date']) );

        $options = array(
            "key" => (string)$this->mandrill_key,
            "template_name" => ( $lang == 'lt' ) ? $this->template_name_lt : $this->template_name_en,
            "template_content" => array(
                0 => array(
                    "name" => "entry_title",
                    "content" => $this->postFormated['name'][$lang]
                ),
                1 => array(
                    "name" => "entry_text",
                    "content" => $html
                ),
                2 => array(
                    "name" => "entry_date",
                    "content" => $blog_entry_date
                ),
                3 => array(
                    "name" => "entry_author",
                    "content" => $this->postFormated['author']
                )
            ),
            "message" => array(
                "html" => $html,
                "text" => strip_tags($html),
                "subject" => $this->postFormated['name_subject'][$lang],
                "from_email" => "info@inkagency.lt",
                "from_name" => "INK agency",
                "to" => $to,
                "headers" => array(
                    "Reply-To" => "info@inkagency.lt"
                ),
                "track_opens" => true,
                "track_clicks" => true,
                "auto_text" => true,
                "url_strip_qs" => true,
                "preserve_recipients" => true,
                "merge" => true,
                "global_merge_vars" => array(),
                "merge_vars" => array(),
                "tags" => array(),
                "google_analytics_domains" => array(),
                "google_analytics_campaign" => "...",
                "metadata" => array(),
                "recipient_metadata" => array(),
                "attachments" => array(),
                "images" => array(
                    0 => array (
                        "type" => "image/jpeg",
                        "name" => "blog_image",
                        "content" => $image_prepared
                    ),
                    1 => array (
                        "type" => "image/jpeg",
                        "name" => "header_image",
                        "content" => $image_prepared_header
                    )
                ),				
            ),
            "async" => false,
            "ip_pool" => "Main",
            "send_at" => ""
        );

        return array( 'headers' => $headers, 'options' => $options );

    }

    /**
     * Real send
     * @return boolean true on success
     */
    private function sendNewsletters( $options )
    {        
        $url = "https://mandrillapp.com/api/1.0/messages/send-template.json";

        $request = \Requests::request( $url, $options['headers'], $options['options'], 'POST', array('timeout'=>5000) );

        if( $request->status_code == 200 && $request->success == 1 )
            return true;
        else
        {   
            $this->setLog( $request );
            return false;
        }

    }

    /**
     * Get author
     * @return string author formated name
     */
    private function getAuthor()
    {
        $author_id = get_field('author', $this->postid);
        
        if( (int)$author_id > 0 )
            $author = get_post( $author_id );

        if( $author->ID > 0 )
            return $author->post_title;
        else
            return '';
    }

    /**
     * Set email recipients
     * @param string $lang prefix
     * @param boolean true if test
     * @return array list of email
     */
    private function setToParam( $lang, $is_test )
    {
        if( $is_test === true )
        {
            $to = array( 
                        0 => array(
                            'email' => $this->email, 
                            'type' => 'cc'
                        ) 
                    );
        }
        else
        {
            if( $lang == 'lt' )
                $to = $this->member_lt;

            if( $lang == 'en' )
                $to = $this->member_en;
        }

        return $to;
    }
    
}
