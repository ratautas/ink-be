<?php
  if (have_rows('misc', 'options')): while (have_rows('misc', 'options')) : the_row();
  $term = get_queried_object();
  $meta_image = get_sub_field('meta_image');
  endwhile; endif;
  $subtitle = get_option('blogname');
  $title = get_the_title();
  $description = get_field('meta_description') ? get_field('meta_description') : get_option('blogdescription');
  $description = $term ? get_field('meta_description', $term) : get_option('blogdescription');
  $image = get_field('meta_image') ? get_field('meta_image') : $meta_image;
  if ($term) { // if category page
      $description = get_field('meta_description', $term) ? get_field('meta_description', $term) : $description;
      $image = get_field('meta_image', $term) ? get_field('meta_image', $term) : $image;
  }
  $permalink = get_the_permalink();
  $theme_url = get_template_directory_uri();
?>
<!DOCTYPE html>
<html
  lang="<?php echo apply_filters('wpml_current_language', null); ?>">

<head>

  <title>
    <?=$title.' | '.$subtitle; ?>
  </title>

  <meta charset="utf-8">
  <meta property="og:type" content="article">
  <meta name="robots" content="index, follow">
  <meta name="description"
    content="<?=$description; ?>" />
  <meta property="og:description"
    content="<?=$description; ?>">
  <meta name="twitter:description"
    content="<?=$description; ?>">
  <meta property="og:title"
    content="<?=$title; ?>">
  <meta name="twitter:title"
    content="<?=$title; ?>">
  <meta property="og:url" content=<?=$permalink; ?>>
  <meta property="og:site_name"
    content="<?=$subtitle; ?>">
  <meta property="og:image"
    content="<?=$image; ?>">
  <meta name="twitter:image"
    content="<?=$image; ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="user-scalable=yes, width=device-width, initial-scale=1">
  <meta name="msapplication-tap-highlight" content="no" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="msapplication-TileImage"
    content="<?=$theme_url; ?>/assets/img/webapp/ms-icon-144x144.png">
  <link rel="shortcut icon"
    href="<?=$theme_url; ?>/assets/img/webapp/favicon.ico"
    type="image/x-icon">
  <link rel="icon"
    href="<?=$theme_url; ?>/assets/img/webapp/favicon.ico"
    type="image/x-icon">
  <link rel="apple-touch-icon" sizes="180x180"
    href="<?=$theme_url; ?>/assets/img/webapp/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32"
    href="<?=$theme_url; ?>/assets/img/webapp/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16"
    href="<?=$theme_url; ?>/assets/img/webapp/favicon-16x16.png">
  <link rel="mask-icon"
    href="<?=$theme_url; ?>/assets/img/webapp/safari-pinned-tab.svg"
    color="#EA3824">
  <meta name="msapplication-TileColor" content="#EA3824">
  <meta name="theme-color" content="#EA3824">
  <link rel="manifest"
    href="<?=$theme_url; ?>/assets/img/webapp/manifest.json" />

  <?php include 'loader.php'; ?>
  
  <?php if(is_user_logged_in()): ?>
  <script>
    var nonce = "<?php echo wp_create_nonce('wp_rest'); ?>";
  </script>
  <?php endif; ?>

  <?php wp_head(); ?>
</head>

<body data-root="<?php echo get_home_url(); ?>"
  data-cat="<?=$meta_description; ?>"
  data-lang="<?php echo apply_filters('wpml_current_language', null); ?>">
  <div class="app">
    <h1>
      <?=$title; ?>
    </h1>
    <p>
      <?=$description; ?>
    </p>
  </div>
  <div class="loader" data-loader>
    <div class="loader__logo">
      <i class="loader__i">
        <svg xmlns="http://www.w3.org/2000/svg" class="loader__svg" viewBox="0 0 69 69">
          <rect x="30.1211" y="23.403" width="9.0142" height="34.3085" fill="#ea3824" />
        </svg>
      </i>
      <i class="loader__n">
        <svg xmlns="http://www.w3.org/2000/svg" class="loader__svg" viewBox="0 0 69 69">
          <polygon
            points="35.565 57.713 22.847 37.729 22.847 57.71 14.337 57.71 14.337 23.403 22.947 23.403 35.665 43.387 35.665 23.403 44.175 23.403 44.175 57.71 35.565 57.713"
            fill="#ea3824" />
        </svg>
      </i>
      <i class="loader__k">
        <svg xmlns="http://www.w3.org/2000/svg" class="loader__svg" viewBox="0 0 69 69">
          <path
            d="M14.34,46.025V11.718h9.014V24.537a6.1886,6.1886,0,0,0,3.082-.748,7.7365,7.7365,0,0,0,2.2769-2.6l5.6118-9.467h9.7178l-5.7119,9.367c-.8638,1.436-1.6709,2.679-2.4,3.73s-1.437,1.974-2.1,2.779L45.1377,46.02H34.7725L26.5542,32.203a14.8622,14.8622,0,0,1-1.58.226c-.5542.05-1.0918.075-1.63.075V46.025Z"
            fill="#ea3824" />
        </svg>
      </i>
      <i class="loader__dot">
        <svg xmlns="http://www.w3.org/2000/svg" class="loader__svg" viewBox="0 0 69 69">
          <path d="M31.019,44.513a5.0743,5.0743,0,1,1,3.6,1.5A4.9238,4.9238,0,0,1,31.019,44.513Z"
            fill="#ea3824" />
        </svg>
      </i>
    </div>
  </div>