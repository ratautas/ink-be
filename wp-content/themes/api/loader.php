<style>
  .loader {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: #fff;
    z-index: 9
  }

  .loader__logo {
    position: absolute;
    top: calc(50% - 69px);
    left: calc(50% - 69px);
    width: 13.8rem;
    height: 13.8rem
  }

  .loader__svg {
    display: block;
    width: 100%;
    height: 100%
  }

  .loader__i {
    -webkit-animation: i 6s ease-in-out infinite;
    animation: i 6s ease-in-out infinite;
    left: 0
  }

  .loader__i,
  .loader__n {
    width: 69px;
    height: 69px;
    position: absolute;
    display: block;
    top: 0
  }

  .loader__n {
    -webkit-animation: n 6s ease-in-out infinite;
    animation: n 6s ease-in-out infinite;
    right: 0
  }

  .loader__k {
    -webkit-animation: k 6s ease-in-out infinite;
    animation: k 6s ease-in-out infinite;
    right: 0
  }

  .loader__dot,
  .loader__k {
    width: 69px;
    height: 69px;
    position: absolute;
    display: block;
    bottom: 0
  }

  .loader__dot {
    -webkit-animation: dot 6s ease-in-out infinite;
    animation: dot 6s ease-in-out infinite;
    left: 0
  }

  @-webkit-keyframes i {
    0% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    15% {
      -webkit-transform: translate3d(70px, 0, 0);
      transform: translate3d(70px, 0, 0)
    }

    25% {
      -webkit-transform: translate3d(70px, 0, 0);
      transform: translate3d(70px, 0, 0)
    }

    40% {
      -webkit-transform: translate3d(70px, 54px, 0);
      transform: translate3d(70px, 54px, 0)
    }

    50% {
      -webkit-transform: translate3d(70px, 54px, 0);
      transform: translate3d(70px, 54px, 0)
    }

    65% {
      -webkit-transform: translate3d(0, 54px, 0);
      transform: translate3d(0, 54px, 0)
    }

    75% {
      -webkit-transform: translate3d(0, 54px, 0);
      transform: translate3d(0, 54px, 0)
    }

    90% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    to {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }
  }

  @keyframes i {
    0% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    15% {
      -webkit-transform: translate3d(70px, 0, 0);
      transform: translate3d(70px, 0, 0)
    }

    25% {
      -webkit-transform: translate3d(70px, 0, 0);
      transform: translate3d(70px, 0, 0)
    }

    40% {
      -webkit-transform: translate3d(70px, 54px, 0);
      transform: translate3d(70px, 54px, 0)
    }

    50% {
      -webkit-transform: translate3d(70px, 54px, 0);
      transform: translate3d(70px, 54px, 0)
    }

    65% {
      -webkit-transform: translate3d(0, 54px, 0);
      transform: translate3d(0, 54px, 0)
    }

    75% {
      -webkit-transform: translate3d(0, 54px, 0);
      transform: translate3d(0, 54px, 0)
    }

    90% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    to {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }
  }

  @-webkit-keyframes n {
    0% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    15% {
      -webkit-transform: translate3d(0, 54px, 0);
      transform: translate3d(0, 54px, 0)
    }

    25% {
      -webkit-transform: translate3d(0, 54px, 0);
      transform: translate3d(0, 54px, 0)
    }

    40% {
      -webkit-transform: translate3d(-54px, 54px, 0);
      transform: translate3d(-54px, 54px, 0)
    }

    50% {
      -webkit-transform: translate3d(-54px, 54px, 0);
      transform: translate3d(-54px, 54px, 0)
    }

    65% {
      -webkit-transform: translate3d(-54px, 0, 0);
      transform: translate3d(-54px, 0, 0)
    }

    75% {
      -webkit-transform: translate3d(-54px, 0, 0);
      transform: translate3d(-54px, 0, 0)
    }

    90% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    to {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }
  }

  @keyframes n {
    0% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    15% {
      -webkit-transform: translate3d(0, 54px, 0);
      transform: translate3d(0, 54px, 0)
    }

    25% {
      -webkit-transform: translate3d(0, 54px, 0);
      transform: translate3d(0, 54px, 0)
    }

    40% {
      -webkit-transform: translate3d(-54px, 54px, 0);
      transform: translate3d(-54px, 54px, 0)
    }

    50% {
      -webkit-transform: translate3d(-54px, 54px, 0);
      transform: translate3d(-54px, 54px, 0)
    }

    65% {
      -webkit-transform: translate3d(-54px, 0, 0);
      transform: translate3d(-54px, 0, 0)
    }

    75% {
      -webkit-transform: translate3d(-54px, 0, 0);
      transform: translate3d(-54px, 0, 0)
    }

    90% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    to {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }
  }

  @-webkit-keyframes k {
    0% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    15% {
      -webkit-transform: translate3d(-54px, 0, 0);
      transform: translate3d(-54px, 0, 0)
    }

    25% {
      -webkit-transform: translate3d(-54px, 0, 0);
      transform: translate3d(-54px, 0, 0)
    }

    40% {
      -webkit-transform: translate3d(-54px, -54px, 0);
      transform: translate3d(-54px, -54px, 0)
    }

    50% {
      -webkit-transform: translate3d(-54px, -54px, 0);
      transform: translate3d(-54px, -54px, 0)
    }

    65% {
      -webkit-transform: translate3d(0, -54px, 0);
      transform: translate3d(0, -54px, 0)
    }

    75% {
      -webkit-transform: translate3d(0, -54px, 0);
      transform: translate3d(0, -54px, 0)
    }

    90% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    to {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }
  }

  @keyframes k {
    0% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    15% {
      -webkit-transform: translate3d(-54px, 0, 0);
      transform: translate3d(-54px, 0, 0)
    }

    25% {
      -webkit-transform: translate3d(-54px, 0, 0);
      transform: translate3d(-54px, 0, 0)
    }

    40% {
      -webkit-transform: translate3d(-54px, -54px, 0);
      transform: translate3d(-54px, -54px, 0)
    }

    50% {
      -webkit-transform: translate3d(-54px, -54px, 0);
      transform: translate3d(-54px, -54px, 0)
    }

    65% {
      -webkit-transform: translate3d(0, -54px, 0);
      transform: translate3d(0, -54px, 0)
    }

    75% {
      -webkit-transform: translate3d(0, -54px, 0);
      transform: translate3d(0, -54px, 0)
    }

    90% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    to {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }
  }

  @-webkit-keyframes dot {
    0% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    15% {
      -webkit-transform: translate3d(0, -54px, 0);
      transform: translate3d(0, -54px, 0)
    }

    25% {
      -webkit-transform: translate3d(0, -54px, 0);
      transform: translate3d(0, -54px, 0)
    }

    40% {
      -webkit-transform: translate3d(70px, -54px, 0);
      transform: translate3d(70px, -54px, 0)
    }

    50% {
      -webkit-transform: translate3d(70px, -54px, 0);
      transform: translate3d(70px, -54px, 0)
    }

    65% {
      -webkit-transform: translate3d(70px, 0, 0);
      transform: translate3d(70px, 0, 0)
    }

    75% {
      -webkit-transform: translate3d(70px, 0, 0);
      transform: translate3d(70px, 0, 0)
    }

    90% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    to {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }
  }

  @keyframes dot {
    0% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    15% {
      -webkit-transform: translate3d(0, -54px, 0);
      transform: translate3d(0, -54px, 0)
    }

    25% {
      -webkit-transform: translate3d(0, -54px, 0);
      transform: translate3d(0, -54px, 0)
    }

    40% {
      -webkit-transform: translate3d(70px, -54px, 0);
      transform: translate3d(70px, -54px, 0)
    }

    50% {
      -webkit-transform: translate3d(70px, -54px, 0);
      transform: translate3d(70px, -54px, 0)
    }

    65% {
      -webkit-transform: translate3d(70px, 0, 0);
      transform: translate3d(70px, 0, 0)
    }

    75% {
      -webkit-transform: translate3d(70px, 0, 0);
      transform: translate3d(70px, 0, 0)
    }

    90% {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }

    to {
      -webkit-transform: translateZ(0);
      transform: translateZ(0)
    }
  }
</style>