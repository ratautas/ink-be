<?php
// add MailChimp namespacing
use DrewM\MailChimp\MailChimp;

use Api\MailChimper\MailChimper;

// upon API theme activation, add Home page and enable some plugins
add_action('after_switch_theme', 'api_setup');

function api_setup()
{
    $home_page = array(
        'post_title' => 'Home page',
        'post_content' => 'Home page content',
        'post_status' => 'publish',
        'post_type' => 'page',
        'page_template' => 'templates/home.php',
    );
    wp_insert_post($home_page);
    $home_page = get_page_by_title('Home Page');
    update_option('page_on_front', $home_page->ID);
    update_option('show_on_front', 'page');
    run_activate_plugin('advanced-custom-fields-pro/acf.php');
    run_activate_plugin('acf-to-rest-api/class-acf-to-rest-api.php');
    run_activate_plugin('duplicate-page/duplicatepage.php');
    run_activate_plugin('disable-emojis/disable-emojis.php');
    run_activate_plugin('classic-editor/classic-editor.php');
    run_activate_plugin('safe-svg/safe-svg.php');
    wp_dequeue_style('wp-block-library');

    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure('/%postname%/');

    //Registering Navigation Menus
    // register_nav_menu('header',__( 'Header Menu', 'api' ));
}

function run_activate_plugin($plugin)
{
    $current = get_option('active_plugins');
    $plugin = plugin_basename(trim($plugin));
    if (!in_array($plugin, $current)) {
        $current[] = $plugin;
        sort($current);
        do_action('activate_plugin', trim($plugin));
        update_option('active_plugins', $current);
        do_action('activate_'.trim($plugin));
        do_action('activated_plugin', trim($plugin));
    }

    return null;
}

add_action('after_setup_theme', 'register_my_menu');

function register_my_menu()
{
    // create menu location
    register_nav_menu('header', __('Header Menu', 'api'));

    // create menu
    wp_create_nav_menu('header');

    $menu_header = get_term_by('name', 'header', 'nav_menu');
    $menu_header_id = $menu_header->term_id;
    if (0 == $menu_header_id) {
        $menu_header_id = wp_create_nav_menu('header');
    }
    $locations = get_theme_mod('nav_menu_locations');
    $locations['header'] = $menu_header_id;
    set_theme_mod('nav_menu_locations', $locations);
}

// import custom post types
require get_template_directory().'/types/member.php';
require get_template_directory().'/types/position.php';
require get_template_directory().'/types/case.php';
require get_template_directory().'/types/service.php';

// add app scripts and styles
function add_scripts()
{
    wp_enqueue_style('app', get_template_directory_uri().'/assets/css/app.css', false, '1.0', 'all');
    wp_enqueue_script('app', get_template_directory_uri().'/assets/js/app.js#asyncload', array(), '1.0', true);
}

add_action('wp_enqueue_scripts', 'add_scripts');

// make scripts async
function async_scripts($url)
{
    if (false === strpos($url, '#asyncload')) {
        return $url;
    } elseif (is_admin()) {
        return str_replace('#asyncload', '', $url);
    } else {
        return str_replace('#asyncload', '', $url)."' async='async";
    }
}

add_filter('clean_url', 'async_scripts', 11, 1);

// remove comments from admin menu
function remove_menus()
{
    remove_menu_page('edit-comments.php');
}

add_action('admin_menu', 'remove_menus');

function get_menu()
{
    $menu = 'header';
    if (($locations = get_nav_menu_locations()) && isset($locations['header'])) {
        $menu = wp_get_nav_menu_object($locations['header']);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list = '';
    }

    return $menu_items;
}

function get_en_menu()
{
    $menu = 'header-en';
    if (($locations = get_nav_menu_locations()) && isset($locations['header'])) {
        $menu = wp_get_nav_menu_object($locations['header']);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list = '';
    }

    return $menu_items;
}

function get_options_fields()
{
    $option_fields = get_fields('options');
    $option_fields['title'] = get_option('blogname');
    $option_fields['description'] = get_option('blogdescription');

    return $option_fields;
}

add_action( 'phpmailer_init', 'mailer_config', 10, 1);
function mailer_config(PHPMailer $mailer)
{
  $option_fields = get_fields('options');
  $email_to = $option_fields['form']['email_to'];

  $mailer->IsSMTP();
  $mailer->Mailer = "smtp";
  $mailer->Host = "itprojektai.hostingas.lt";
  $mailer->SMTPSecure = 'ssl';
  $mailer->SMTPAuth = true;
  $mailer->Username = 'sender@inkagency.lt'; // system smtp box
  $mailer->Password = 'BeselenisKnygotyra';
  $mailer->From = 'sender@inkagency.lt';
  $mailer->FromName = 'INK agency';
  $mailer->Port = 465;
  $mailer->SMTPDebug = 0;
  $mailer->CharSet  = "utf-8";
  if( $email_to )
    $mailer->AddReplyTo( $email_to, 'INK agency' );
  else
    $mailer->AddReplyTo( 'sender@inkagency.lt', 'INK agency' );
}

add_action('rest_api_init', function () {
    register_rest_route('wp/v2', '/nav', array(
    'methods' => 'GET',
    'callback' => 'get_menu',
  ));

    register_rest_route('wp/v2', '/nav/en', array(
    'methods' => 'GET',
    'callback' => 'get_en_menu',
    ));

    register_rest_route('wp/v2', '/options', array(
    'methods' => 'GET',
    'callback' => 'get_options_fields',
  ));

    register_rest_field(
      'post',
      'item_lang',
    array(
        'get_callback' => 'rest_get_lang',
        'update_callback' => null,
        'schema' => null,
     )
  );

    register_rest_route('api', '/subscribe', array(
      array(
          'methods' => WP_REST_Server::CREATABLE,
          'callback' => 'add_subscriber',
      ),
  ));

    register_rest_route('api', '/apply', array(
          array(
              'methods' => WP_REST_Server::CREATABLE,
              'callback' => 'add_application',
          ),
      ));

    register_rest_route('api', '/mailchimp-send', array(
          array(
              'methods' => 'POST',
              'callback' => 'add_mailchimp_send',
          ),
      ));

});

if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

function add_subscriber($data)
{
    require get_template_directory().'/MailChimp.php';

    $mailchimp_options = get_field('mailchimp', 'option');
    $form_options = get_field('form', 'option');

    if ($data['email']) {
        $MailChimp = new MailChimp($mailchimp_options['key']);
        $list_id = ( ICL_LANGUAGE_CODE == 'lt' ) ? $mailchimp_options['id'] : $mailchimp_options['id_en'];
        $result = $MailChimp->post("lists/$list_id/members", [
                'email_address' => $data['email'],
                'status' => 'subscribed',
                ]);
        if (!$result) {
            $data = array('success' => 0, 'status' => 500, 'detail' => $form_options['mailchimp_error']);
            return new WP_REST_Response($data, 400);
        } else {
            return $result;
        }
    } else {
        $data = array('success' => 0, 'status' => 400, 'detail' => $form_options['no_email_error']);

        return new WP_REST_Response($data, 400);
    }
}

function add_mailchimp_send($data)
{ 

  require get_template_directory().'/MailChimper.php';
  
  $action = $data['action'];
  $postid = (int)$data['postid'];
  $email = $data['email'];

  if( $postid )
  {
    $MCMail = new MailChimper( $action, $postid, $email );
    $mailChimper = $MCMail->run();
    
    return new WP_REST_Response( array( 'result' => $mailChimper['status'], 'message' => $mailChimper['message'] ), 200 );
  }

}

function mailchimp_add_metabox() {
  add_meta_box(
      'mailchimp_add_metabox01',
      'Siųsti mailchimp naujienlaiškį',
      'mailchimp_add_metabox_html',
      'post',
      'side'
  );
}

add_action( 'add_meta_boxes', 'mailchimp_add_metabox' );

function mailchimp_add_metabox_html() 
{
  global $post;

  $status = get_field('mailchimp_sended', $post->ID);
  if( is_array($status) && !empty($status[0]) && $status[0] == 'sended' )
  {
    $html = 'Ši naujiena jau išsiųsta.<br/>';
    $html .= 'Jeigu norite siųsti pakartotinai - naujienos apačioje nuimkite varnelę.';
  }
  else
  {
    $html = 'Nurodykite testinį el. pašto adresą';
    $html .= '<input type="text" name="mailchimp-email" class="input" data-mc-email >';
    $html .= '<br/><br/>';
    $html .= '<a href="#" class="button button-primary" data-mc-test>Testinis siuntimas</a>';
    $html .= '<br/><br/>';
    $html .= '<a href="#" class="button button-primary" data-mc-send>Siųsti naujienlaiškį prenumeratoriams</a>';
    $html .= '<div data-mc-message></div>';
    $html .= get_mailchimp_scripts();
  }

  print $html;
}

function get_mailchimp_scripts()
{
  global $post;

  $script = '<script>';
  $script .= 'jQuery(function($){';
  $script .= '
      $("[data-mc-test]").on("click", function(e){
        e.preventDefault();
        if(  confirm("Siųsti testinį naujienlaiškį?") ) {

          $("[data-mc-email]").css({ "borderColor" : "#ddd" })
          var email = $("[data-mc-email]").val();
          var regex = /\S+@\S+\.\S+/;
          if( regex.test(email) ) {
            $("[data-mc-message]").html("<br/>Palaukite, vykdomas siuntimas...").fadeIn(200);
            $("[data-mc-test]").fadeOut(800);
            $.post(
              "'.get_bloginfo('url').'/wp-json/api/mailchimp-send",
              {
                  action: "mc-test",
                  postid: "'.$post->ID.'",
                  email: email
              },
              function( response ){
                $("[data-mc-test]").fadeIn(800);
                if( response.result === true ) {
                    $("[data-mc-message]").html("<br/>"+response.message).fadeIn(800);
                    $("[data-mc-email]").val("");
                    setTimeout(function(){
                      $("[data-mc-message]").html("").fadeOut(1000);
                    },6000);
                } else {
                  $("[data-mc-message]").html("<br/>"+response.message).fadeIn(800);
                  setTimeout(function(){
                    $("[data-mc-message]").html("").fadeOut(5000);
                  },6000);
                }
              }
            );
            
          }
          else {
            $("[data-mc-email]").css({ "borderColor" : "red" });
          }

        }
        
      });
      $("[data-mc-send]").on("click", function(e){
        e.preventDefault();
        if(  confirm("Siųsti naujienlaiškius visiems gavėjams?") ) {

          $("[data-mc-message]").html("<br/>Palaukite, vykdomas siuntimas...").fadeIn(200);
          $("[data-mc-send]").fadeOut(800);
          $.post(
            "'.get_bloginfo('url').'/wp-json/api/mailchimp-send",
            {
                action: "mc-send",
                postid: "'.$post->ID.'",
                email: ""
            },
            function( response ){
              $("[data-mc-send]").fadeIn(800);
              if( response.result === true ) {
                  $("[data-mc-message]").html("<br/>"+response.message).fadeIn(800);
                  setTimeout(function(){
                    $("[data-mc-message]").html("").fadeOut(60000);
                  },6000);
              } else {
                $("[data-mc-message]").html("<br/>"+response.message).fadeIn(800);
                setTimeout(function(){
                  $("[data-mc-message]").html("").fadeOut(5000);
                },6000);
              }
            }
          );

        }
        
      });
  ';
  $script .= '})';
  $script .= '</script>';

  return $script;
}

function rest_get_lang($object)
{
    // if (qtranxf_getLanguage()) {
    //     return qtranxf_getLanguage();
    // }
    if (function_exists('qtranxf_getLanguage')) {
        return qtranxf_getLanguage();
    }
}

///////////////////////////
//////// API PART /////////
///////////////////////////

$GLOBALS['applications_table'] = $wpdb->prefix.'api_applications';

// Add menu items
function add_drafts_admin_menu_item()
{
    //$page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position
    add_menu_page('Applications', 'Applications', 'manage_options', 'applications', 'render_applications', 'dashicons-media-spreadsheet', 1);
}
add_action('admin_menu', 'add_drafts_admin_menu_item');

// CREATE TABLE `ink`.`api_applications` ( `id` INT NOT NULL AUTO_INCREMENT , `value1` TEXT NOT NULL , `comment` TEXT NOT NULL , INDEX `id` (`id`)) ENGINE = InnoDB;

//Winners: http://localhost:8888/meister/wp-json/wp/v2/pages/54

// DOMAIN/wp-json/contacts/v1/push

global $wpdb;
$applications_table = $GLOBALS['applications_table'];

if ($wpdb->get_var("SHOW TABLES LIKE '$applications_table'") != $applications_table) {
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $applications_table (
         id int(6) NOT NULL AUTO_INCREMENT,
         position text NOT NULL,
         name text NOT NULL,
         email text NOT NULL,
         tel text NOT NULL,
         message text NOT NULL,
         cv text NOT NULL,
         ip text NOT NULL,
         time text NOT NULL,
         UNIQUE KEY id (id)
    ) $charset_collate;";

    require_once ABSPATH.'wp-admin/includes/upgrade.php';
    dbDelta($sql);
} else {
}

//registered users table with CSV export capability
function render_applications()
{
    global $wpdb;
    $applications_table = $GLOBALS['applications_table'];
    $entries = $wpdb->get_results("SELECT * FROM $applications_table"); ?>
<div class="wrap">
  <h2>Applications (Total:
    <?php echo count($entries); ?>)</h2>

  <table class="wp-list-table widefat fixed pages">
    <thead>
      <tr>
        <td>Position</td>
        <td>Name</td>
        <td>Email</td>
        <td>Tel</td>
        <td>Message</td>
        <td>CV</td>
        <td>IP</td>
        <td>Time</td>
      </tr>
    </thead>

    <tbody id="the-list">
      <?php foreach ($entries as $entry) : ?>
      <tr>
        <td>
          <?php echo $entry->position; ?>
        </td>
        <td>
          <?php echo $entry->name; ?>
        </td>
        <td>
          <?php echo $entry->email; ?>
        </td>
        <td>
          <?php echo $entry->tel; ?>
        </td>
        <td>
          <?php echo $entry->message; ?>
        </td>
        <td>
          <?php if ($entry->cv) {
        echo '<a href="'.$entry->cv.'" target=_blank" download>CV</a>';
    } ?>
        </td>
        <td>
          <?php echo $entry->ip; ?>
        </td>
        <td>
          <?php echo $entry->time; ?>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>

    <tfoot>
      <tr>
        <td>Position</td>
        <td>Name</td>
        <td>Email</td>
        <td>Tel</td>
        <td>Message</td>
        <td>CV</td>
        <td>IP</td>
        <td>Time</td>
      </tr>
    </tfoot>
  </table>
  <!-- <button style="margin-top: 1em" onclick="myAjax()">EXPORT to CSV</button> -->
</div>

<script type="text/javascript">
  function myAjax() {
    var ajax_url = "<?=admin_url('admin-ajax.php'); ?>";
    jQuery.ajax({
      type: "POST",
      url: ajax_url,
      data: {
        action: 'exportCsv'
      },
      success: function (data) {

        var link = document.createElement('a');
        var currentTime = new Date();
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();
        var fullTime = month + '-' + day + '-' + hours + '' + minutes;
        link.download = fullTime + '-registracijos.csv';
        link.href = 'data:,' + encodeURIComponent(data);
        link.click();
      }
    });
  }
</script>
<?php
}

function get_entries_all()
{
    global $wpdb;
    $applications_table = 'api_applications';
    $all_entries = $wpdb->get_results("SELECT * FROM $applications_table");

    return $all_entries;
}

function exportCsv()
{
    $host = 'localhost';
    $user = DB_USER; //predefined in wp-config.php
    $pass = DB_PASSWORD; //predefined in wp-config.php
    $db = DB_NAME;
    $applications_table = 'api_applications'; //chould be created separately

    $file = 'export';

    $link = mysql_connect($host, $user, $pass) or die('Cannot connect.'.mysql_error());
    mysql_set_charset('utf8', $link);

    mysql_select_db($db) or die('Cannot connect.');

    $result = mysql_query('SHOW COLUMNS FROM '.$applications_table.'');
    $i = 0;
    if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_assoc($result)) {
            $csv_output .= $row['Field'].'; ';
            ++$i;
        }
    }
    $csv_output .= "\n";

    $values = mysql_query('SELECT * FROM '.$applications_table.'');
    while ($rowr = mysql_fetch_row($values)) {
        for ($j = 0; $j < $i; ++$j) {
            $csv_output .= $rowr[$j].'; ';
        }
        $csv_output .= "\n";
    }

    $filename = $file.'_'.date('Y-m-d_H-i', time());
    echo $csv_output;

    exit;
}

add_action('wp_ajax_exportCsv', 'exportCsv');
add_action('wp_ajax_nopriv_exportCsv', 'exportCsv');

// add_action('rest_api_init', function () {
// });

function get_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

function add_application($data)
{
    global $wpdb;

    $applications_table = $GLOBALS['applications_table'];

    $position = sanitize_text_field($data['position']);
    $name = sanitize_text_field($data['name']);
    $email = sanitize_email($data['email']);
    $tel = sanitize_text_field($data['tel']);
    $message = sanitize_text_field($data['message']);
    $cv = $data['cv'];
    $ip = get_ip();
    $time = date('Y-m-d h:i:s');

    $option_fields = get_fields('options');
    $email_to = $option_fields['form']['email_to'];
    
    if (!$name) {
        $error = $option_fields['form']['no_name_error'];
    }
    if (!$email) {
        $error = $option_fields['form']['no_email_error'];
    }

    $check = $wpdb->get_results("SELECT id FROM $applications_table WHERE ip = '".$ip."' AND time BETWEEN '".date('Y-m-d')." 00:00:00' AND '".date('Y-m-d')." 23:59:59'");
    if (!count($check)) {
        if ($name && $email) {
            require_once ABSPATH.'wp-admin/includes/image.php';
            require_once ABSPATH.'wp-admin/includes/file.php';
            require_once ABSPATH.'wp-admin/includes/media.php';

            $cv_id = media_handle_upload('cv', 0);
            $cv = wp_get_attachment_url($cv_id);

            $wpdb->show_errors();

            $result = $wpdb->insert(
             $applications_table,
             array(
               'position' => $position,
               'name' => $name,
               'email' => $email,
               'tel' => $tel,
               'message' => $message,
               'cv' => $cv,
               'ip' => $ip,
               'time' => $time,
             ),
             array(
               '%s',
               '%s',
               '%s',
               '%s',
               '%s',
               '%s',
               '%s',
               '%s',
             )
           );

            if ($result) {
                $headers = array('Content-Type: text/html; charset=UTF-8');

                $to_admin = $email_to;
                $subject_admin = 'Nauja paraiška: ('.$position.') - '.$name;
                $body_admin = '<b>Vardas: </b>'.$name.
                '<br><b>Pavardė: </b>'.$email.
                '<br><b>Telefonas: </b>'.$tel.
                '<br><b>Žinutė: </b>'.$message.
                '<br><b>CV: </b>'.$cv.'>';
                wp_mail($to_admin, $subject_admin, $body_admin, $headers);
                $data = array('success' => 1, 'status' => 200, 'detail' => $result);

                return new WP_REST_Response($data, 200);
            } else {
                $data = array('success' => 0, 'status' => 500, 'detail' => $option_fields['form']['application_error']);

                return new WP_REST_Response($data, 500);
            }
        } else {
            $data = array('success' => 0, 'status' => 400, 'detail' => $error);

            return new WP_REST_Response($data, 400);
        }
    } else {
        $data = array('success' => 0, 'status' => 400, 'detail' => $option_fields['form']['ip_error']);

        return new WP_REST_Response($data, 400);
    }
}

add_filter('wpml_custom_field_original_data', '__return_null');

// stop thumbnail generation:
function add_image_insert_override($sizes){
  unset( $sizes['thumbnail']);
  unset( $sizes['medium']);
  unset( $sizes['large']);
  return $sizes;
}

add_filter('intermediate_image_sizes_advanced', 'add_image_insert_override' );
