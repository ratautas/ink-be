<?php

add_action( 'init', 'register_position_post_type' );

// A custom function that calls register_post_type
function register_position_post_type() {
  $args = array(
    'supports' => array(
      'title',
      'editor',
      'custom-fields',
      // 'page-attributes',
      // 'author',
      // 'thumbnail',
      // 'excerpt',
      // 'trackbacks',
      // 'comments',
      // 'revisions',
      // 'post-formats'
    ), // 'selectively enable various post features such as featured images, excerpts, custom fields, etc. If set to FALSE instead of an array, it disables the editor for this post type – useful if you want to lock all posts of this type while keeping them visible (list of array values is below)',
    'labels' => array(
      'name' => _x('Positions', 'post type general name' ), // general name for the post type, usually plural. The same and overridden by $post_type_object->label. Default is Posts/Pages
      'singular_name' => _x( 'Position', 'post type singular name' ), // name for one object of this post type. Default is Post/Page
      // 'add_new' => _x('Position', 'post type add_new'), // the add new text. The default is "Add New" for both hierarchical and non-hierarchical post types. When internationalizing this string, please use a gettext context matching your post type. Example: _x('Add New', 'product');
      'add_new_item' => _x('Add New Position', 'post type add_new_item'), // Default is Add New Post/Add New Page.
      'edit_item' => _x('Edit Position', 'post type edit_item'), // Default is Edit Post/Edit Page.
      'new_item' => _x('Add New Position', 'post type new_item'), // Default is New Post/New Page.
      // 'view_item' => _x('Position', 'post type view_item'), // Default is View Post/View Page.
      // 'view_items' => _x('Position', 'post type view_items'), // Label for viewing post type archives. Default is 'View Posts' / 'View Pages'.
      // 'search_items' => _x('Position', 'post type search_items'), // Default is Search Posts/Search Pages.
      // 'not_found' => _x('Position', 'post type not_found'), // Default is No posts found/No pages found.
      // 'not_found_in_trash' => _x('Position', 'post type not_found_in_trash'), // Default is No posts found in Trash/No pages found in Trash.
      // 'parent_item_colon' => _x('Position', 'post type parent_item_colon'), // This string isn't used on non-hierarchical types. In hierarchical ones the default is 'Parent Page:'.
      // 'all_items' => _x('Position', 'post type all_items'), // String for the submenu. Default is All Posts/All Pages.
      // 'archives' => _x('Position', 'post type archives'), // String for use with archives in nav menus. Default is Post Archives/Page Archives.
      // 'attributes' => _x('Position', 'post type attributes'), // Label for the attributes meta box. Default is 'Post Attributes' / 'Page Attributes'.
      // 'insert_into_item' => _x('Position', 'post type insert_into_item'), // String for the media frame button. Default is Insert into post/Insert into page.
      // 'uploaded_to_this_item' => _x('Position', 'post type uploaded_to_this_item'), // String for the media frame filter. Default is Uploaded to this post/Uploaded to this page.
      // 'featured_image' => _x('Position', 'post type featured_image'), // Default is Featured Image.
      // 'set_featured_image' => _x('Position', 'post type set_featured_image'), // Default is Set featured image.
      // 'remove_featured_image' => _x('Position', 'post type remove_featured_image'), // Default is Remove featured image.
      // 'use_featured_image' => _x('Position', 'post type use_featured_image'), // Default is Use as featured image.
      // 'menu_name' => _x('Position', 'post type menu_name'), // Default is the same as `name`.
      // 'filter_items_list' => _x('Position', 'post type filter_items_list'), // String for the table views hidden heading.
      // 'items_list_navigation' => _x('Position', 'post type items_list_navigation'), // String for the table pagination hidden heading.
      // 'items_list' => _x('Position', 'post type items_list'), // String for the table hidden heading.
      // 'name_admin_bar' => _x('Position', 'post type name_admin_bar') // String for use in New in Admin menu bar. Default is the same as `singular_name`.
    ), // 'An array that defines various pieces of text, i.e. ‘Add New Post’ can be renamed ‘Add New Movie’. The keys for the labels array are explained below this list.',
    'hierarchical' => false, // 'whether posts can be assigned to a Parent post, if TRUE, the $supports array must contain the feature ‘page-attributes’',
    'exclude_from_search' => false, // 'Whether posts of this type appear in normal search results. The default value is the opposite of public’s value.',
    'publicly_queryable' => true, // 'Whether posts of this type can be retrieved using an URL such as http://www.mywebsite.com/?post_type=movie, or in advanced usage, via the query_posts() function. The default value is public’s value',
    'show_ui' => true, // 'Whether the menu links and post editor are visible in the Admin Control Panel. The default value is public’s value.',
    'show_in_nav_menus' => true, //  'Whether posts of this type can be added to navigation menus created via the Appearance -> Menus screen. The default value is public’s value.',
    'show_in_menu' =>  true, // 'Where the post type link appears in the Admin Control Panel navigation. FALSE hides the link. TRUE adds the link as a new top-level link. Entering a string allows you to place link as sub-link of existing top-level link i.e. entering options-general.php places it under the Settings link.',
    'show_in_admin_bar' => true, // 'Whether this post type appears in the upper Admin bar, under the + New link',
    'menu_position' => 21, // 'Position of the new link in the Admin Control Panel navigation menu, 5 puts it below Posts, 100 puts it below Settings, visit the WordPress Codex entry for the full list of positions',
    'taxonomies' => array(), // 'an array of taxonomies that can be applied to posts of this type, taxonomies must already be registered – this does not create them!',
    'has_archive' => false, // 'whether the post type has an archive page, the url follows your permalink structure, and the slug is the name you entered in parameter 1 of register_post_types() i.e. http://www.mywebsite.com/movie_reviews/ shows all movie_review posts.'
    'query_var' => true, // TRUE or FALSE sets whether a post can be viewed by typing the post type and post name as a query in the URL i.e. ‘http://www.mywebsite.com/?movie=the-matrix‘. If you enter a string of text, you can set the text to use after the ? character, so entering ‘film’ would result in ‘?film=the-matrix‘ instead.,
    'description' => 'Agency positions', //A short & descriptive summary of the post type, this can be shown in the post type’s template but is not used anywhere else.
    'public' => true, // Whether the post type is visible to authors and visitors, the default value is FALSE which means it does not even appear in the Admin Control Panel.
    'menu_icon' => 'dashicons-admin-users', // 'dashicons-video-alt' (Uses the video icon from Dashicons[https://developer.wordpress.org/resource/dashicons/#admin-post])
    'show_in_rest' => true, // Whether to expose this post type in the REST API.
    'rewrite' => [
      'slug' => '/',
      'with_front' => false
      ]
    );
    
    // Register the position post type with all the information contained in the $arguments array
    register_post_type( 'position', $args );
  }