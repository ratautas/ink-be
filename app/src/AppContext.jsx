import React from "react";

import * as latinize from "latinize";
import * as Cookies from "js-cookie";
import dayjs from "dayjs";

import lt from "dayjs/locale/lt";

import hasAny from "./utils/hasAny";
import routeUrl from "./utils/routeUrl";

const Context = React.createContext({});

const rootUrl = document.body.dataset.root || "";
const lang = document.body.dataset.lang || "lt";

export const AppContext = Context;

export class AppProvider extends React.Component {
  date = str => {
    const l = {
      ...lt,
      months: [
        "m. sausio",
        "m. vasario",
        "m. kovo",
        "m. balandžio",
        "m. gegužės",
        "m. birželio",
        "m. liepos",
        "m. rugpjūčio",
        "m. rugsėjo",
        "m. spalio",
        "m. lapkričio",
        "m. gruodžio"
      ]
    };
    const locale = lang === "lt" ? l : {};
    const parsed = dayjs(str, { locale }).format("YYYY MMMM D") || "";
    return parsed;
  };

  getPageById = id => {
    return this.state.pagesById[id];
  };

  set = update => {
    this.setState(prevState => ({
      ...prevState,
      ...update
    }));
  };

  openModal = title => {
    this.set({ activeModal: title });
    document.body.style.paddingRight =
      window.innerWidth - document.body.clientWidth + "px";
    document.body.style.touchAction = "none";
    document.body.style.overflow = "hidden";
    document.body.style.pointerEvents = "none";
    document.documentElement.style.paddingRight =
      window.innerWidth - document.body.clientWidth + "px";
    document.documentElement.style.touchAction = "none";
    document.documentElement.style.overflow = "hidden";
    document.documentElement.style.pointerEvents = "none";
  };

  closeModal = () => {
    this.set({ activeModal: null });
    document.body.removeAttribute("style");
    document.documentElement.removeAttribute("style");
  };

  state = {
    rootUrl,
    lang,
    ready: false,
    navOpen: false,
    tranSpeed: 1000,
    pageTransition: false,
    thisPath: null,
    prevPath: null,
    samePath: true,
    posts: null,
    posts2: null,
    posts3: null,
    posts4: null,
    posts5: null,
    pages: null,
    nav: null,
    members: null,
    positions: null,
    cases: null,
    services: null,
    categories: null,
    options: null,
    date: this.date,
    getPageById: this.getPageById,
    set: this.set,
    openModal: this.openModal,
    closeModal: this.closeModal,
    activeModal: null,
    pagesById: {},
    postsById: {},
    membersById: {},
    servicesById: {},
    categoriesById: {},
    searchData: [],
    searchPage: null,
    blogPage: null,
    casesPage: null,
    cookieSet: Cookies.get("cookiebar") || false
  };

  componentDidMount() {
    const l = lang === "lt" ? "" : `/${lang}`;

    let post_status = '';
    if(window.nonce !== undefined){
      post_status = `&status=any&_wpnonce=${window.nonce}`;
    }

    this.fetchData(`/wp-json/wp/v2/pages?per_page=100${post_status}`, "pages");
    this.fetchData(`/wp-json/wp/v2/posts?per_page=100${post_status}`, "posts");
    this.fetchData(`/wp-json/wp/v2/posts?per_page=100&page=2${post_status}`, "posts2");
    this.fetchData(`/wp-json/wp/v2/posts?per_page=100&page=3${post_status}`, "posts3");
    this.fetchData(`/wp-json/wp/v2/posts?per_page=100&page=4${post_status}`, "posts4");
    this.fetchData(`/wp-json/wp/v2/posts?per_page=100&page=5${post_status}`, "posts5");
    this.fetchData(`/wp-json/wp/v2/nav${l}?per_page=100${post_status}`, "nav");
    this.fetchData(`/wp-json/wp/v2/member?per_page=100${post_status}`, "members");
    this.fetchData(`/wp-json/wp/v2/position?per_page=100${post_status}`, "positions");
    this.fetchData(`/wp-json/wp/v2/categories?per_page=100${post_status}`, "categories");
    this.fetchData(`/wp-json/wp/v2/case?per_page=100${post_status}`, "cases");
    this.fetchData(`/wp-json/wp/v2/service?per_page=100${post_status}`, "services");
    this.fetchData(`/wp-json/wp/v2/options`, "options");
  }

  fetchData(url, type) {
    fetch(`${document.body.dataset.root}${url}`)
    .then(res => res.json())
    .then(data => {
      this.updateData({ [type]: data });
      if (this.checkIfReady()) {
        let posts = this.state.posts
          .concat(this.state.posts2.length ? this.state.posts2 : [])
          .concat(this.state.posts3.length ? this.state.posts3 : [])
          .concat(this.state.posts4.length ? this.state.posts4 : [])
          .concat(this.state.posts5.length ? this.state.posts5 : []);

        let pagesById = {};

        let membersById = {};
        this.state.members.map(member => {
          member.posts = [];
          membersById[member.id] = member;
          return member;
        });

        this.state.pages.map(page => {
          if (
            page.template === "templates/career.php" &&
            this.state.positions.length &&
            page.acf.positions &&
            page.acf.positions.length
          ) {
            page.positions = this.state.positions.filter(position => {
              return page.acf.positions.filter(pos => {
                return pos.position.ID === position.id;
              });
            });
          }
          if (page.template === "templates/search.php")
            this.setState({ searchPage: page });
          if (page.template === "templates/cases.php")
            this.setState({ casesPage: page });
          if (page.template === "templates/blog.php")
            this.setState({ blogPage: page });
          pagesById[page.id] = page;
          return page;
        });

        let postsById = {};

        posts.map(post => {
          postsById[post.id] = post;
          return post;
        });

        let servicesById = {};
        if (this.state.services && this.state.services.length) {
          this.state.services.map(service => {
            servicesById[service.id] = service;
            return service;
          });
        }

        let categoriesById = {};
        this.state.categories.map(category => {
          categoriesById[category.id] = category;
          return category;
        });

        posts = posts.map(post => {
          return {
            ...post,
            member: post.acf.author ? membersById[post.acf.author] : null
          };
        });

        let members = this.state.members.map(member => {
          return { ...member, posts: [] };
        });

        this.state.posts.forEach(post => {
          if (post.acf.author) {
            members.forEach((member, m) => {
              if (member.id === post.acf.author) {
                members[m].posts.push(post);
                membersById[member.id].posts.push(post);
              }
            });
          }
        });

        const cases = [];

        this.state.cases.forEach(c => {
          if (c.acf.render) cases.push(c);
        });

        let searchData = [];

        this.state.posts.forEach(post => {
          const f = post.acf;
          const lead =
            hasAny(f.intro) && hasAny(f.intro.lead) ? f.intro.lead : "";
          const text =
            hasAny(f.intro) && hasAny(f.intro.text) ? f.intro.text : "";
          let sectionsContent = "";
          let categories = "";

          hasAny(post.categories) &&
            post.categories.forEach(category => {
              categories =
                categories + " " + categoriesById[category]["name"];
            });

          hasAny(f) &&
            hasAny(f.sections) &&
            f.sections.forEach(section => {
              sectionsContent =
                sectionsContent + " " + section.title + " " + section.text;
            });
          const content = `${post.title.rendered} ${post.content.rendered} ${lead} ${text} ${categories} ${sectionsContent}`;
          searchData.push({
            content: latinize(content)
              .toLowerCase()
              .trim(),
            title: post.title.rendered,
            link: routeUrl(post.link)
          });
        });

        this.state.members.forEach(member => {
          const content = `${member.title.rendered} ${member.content.rendered}`;
          searchData.push({
            content: latinize(content)
              .toLowerCase()
              .trim(),
            title: member.title.rendered,
            link: routeUrl(member.link)
          });
        });

        this.state.cases.forEach(c => {
          const f = c.acf;
          const feedback =
            hasAny(f.feedback) && hasAny(f.feedback.text)
              ? f.feedback.text
              : null;
          const label = hasAny(f.label) ? f.label : null;
          const solutions =
            hasAny(f.solutions) && hasAny(f.solutions.text)
              ? f.solutions.text
              : null;
          const task =
            hasAny(f.task) && hasAny(f.task.text) ? f.task.text : null;
          let victories = "";
          hasAny(f.victories) &&
            hasAny(f.victories.items) &&
            f.victories.items.forEach(victory => {
              victories = victories + " " + victory.label;
            });
          const content = `${c.title.rendered} ${c.content.rendered} ${feedback} ${label} ${solutions} ${task} ${victories}`;
          searchData.push({
            content: latinize(content)
              .toLowerCase()
              .trim(),
            title: c.title.rendered,
            link: routeUrl(c.link)
          });
        });

        this.state.positions.forEach(position => {
          const f = position.acf;
          let blocksContent = "";
          hasAny(f) &&
            hasAny(f.blocks) &&
            f.blocks.forEach(block => {
              blocksContent = blocksContent + " " + block.description;
            });
          const content = `${position.title.rendered} ${position.content.rendered} ${blocksContent}`;
          searchData.push({
            content: latinize(content)
              .toLowerCase()
              .trim(),
            title: position.title.rendered,
            link: routeUrl(position.link)
          });
        });

        this.updateData({
          searchData,
          pagesById,
          postsById,
          membersById,
          categoriesById,
          servicesById,
          posts,
          members,
          cases,
          ready: true
        });
      }
    });
  }

  checkIfReady = () => {
    return (
      this.state.posts &&
      this.state.posts2 &&
      this.state.posts3 &&
      // this.state.posts4 &&
      // this.state.posts5 &&
      this.state.pages &&
      this.state.nav &&
      this.state.categories &&
      this.state.members &&
      this.state.positions &&
      this.state.cases &&
      this.state.services &&
      this.state.options !== null
    );
  };

  updateData = update => {
    this.setState(prevState => ({
      ...prevState,
      ...update
    }));
  };

  render() {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    );
  }
}

export const AppConsumer = Context.Consumer;
