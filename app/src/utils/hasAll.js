const hasAll = (obj) => {
  let state = true;
  if (obj && typeof obj !== 'undefined') {
    if (Array.isArray(obj) && obj.length < 0) {
      state = false;
    } else {
      for (var key in obj) {
        state = !obj[key] || !obj[key].length ? false : state;
      }
    }
  }
  return state;
};

export default hasAll;