const unStyle = ($el) => {
  if ($el) {
    $el.style.transform = '';
    $el.style.msTransform = '';
    $el.style.WebkitTransform = '';
    $el.style.opacity = '';
  }
};

export default unStyle;