const lang = document.body.dataset.lang || 'lt';
const rootUrl = document.body.dataset.root.replace(`/${lang}/`, '');

const routeUrl = (str) => str.replace(rootUrl, '').slice(0, -1);

export default routeUrl;