const modClass = (rootClass, modifier, blockClass) => {
  let classString = blockClass ? `${blockClass} ${rootClass}` : rootClass;
  classString = modifier ? `${classString} ${rootClass}--${modifier}` : classString;
  return classString;
};

export default modClass;