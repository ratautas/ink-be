const bg = (url) => {
  if (url) {
    return {
      backgroundImage: `url('${url}')`,
    };
  }
};

export default bg;