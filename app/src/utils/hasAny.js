const hasAny = (obj) => {
  let state = false;
  if (obj && typeof obj !== 'undefined') {
    if (Array.isArray(obj) && obj.length) {
      state = true;
    } else {
      for (var key in obj) {
        if (obj[key]) {
          state = obj[key] && (obj[key].length || typeof obj[key] !== 'undefined') ? true : state;
        }
      }
    }
  }
  return state;
};

export default hasAny;