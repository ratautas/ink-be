import 'react-app-polyfill/ie9';

import React from 'react';
import ReactDOM from 'react-dom';
// // import 'fg-loadcss';
// import {
//   loadCSS
// } from 'fg-loadcss';

import * as serviceWorker from './serviceWorker';

import './assets/scss/index.scss';
import App from './App.jsx';

// loadCSS(`${document.body.dataset.root}/wp-content/themes/api/assets/css/app.css`);

ReactDOM.render( < App /> , document.querySelector('.app'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();