import React from "react";
import { Router, NavLink } from "react-router-dom";
import createHistory from "history/createBrowserHistory";

import * as Cookies from "js-cookie";
import { TweenLite, CSSPlugin, AttrPlugin, ScrollToPlugin } from "gsap/all";

import { AppProvider, AppConsumer, AppContext } from "./AppContext.jsx";

import AppRouter from "./partials/AppRouter";
import CookieBar from "./partials/CookieBar";
import Langs from "./partials/Langs";
import NavIcon from "./partials/NavIcon";
import Nav from "./partials/Nav";

import { ReactComponent as Logo } from "./assets/img/logo.svg";

// prevent tree shaking
const gsapPlugins = [ScrollToPlugin, CSSPlugin, AttrPlugin];
export { gsapPlugins };

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cookieSet: Cookies.get("cookiebar") || false
    };

    this.setCookie = this.setCookie.bind(this);

    this.routerHistory = createHistory();
    this.routerHistory.listen((location, action) => {});
  }

  setCookie() {
    Cookies.set("cookiebar", true, { expires: 999 });
    this.setState(prevState => ({
      ...prevState,
      cookieSet: true
    }));
  }

  fadeLoader() {
    const $loader = document.querySelector("[data-loader]");
    if ($loader) {
      TweenLite.to($loader, 1.2, {
        opacity: 0,
        onComplete: function() {
          return this.target && this.target.parentNode.removeChild(this.target);
        }
      });
    }
  }

  render() {
    return (
      <AppProvider>
        <AppConsumer>
          {ctx => {
            if (ctx.ready) {
              this.fadeLoader();
              return (
                <Router
                  history={this.routerHistory}
                  ref={el => (this.router = el)}
                >
                  <>
                    {!this.state.cookieSet && (
                      <CookieBar emitAccept={this.setCookie} />
                    )}
                    <div className="app__top" />
                    <NavLink
                      aria-label="INK agency"
                      className="app__logo"
                      to={ctx.lang === "lt" ? "/" : `/${ctx.lang}/`}
                    >
                      <Logo />
                    </NavLink>
                    <NavIcon />
                    <Langs className="app__langs" />
                    <Nav />
                    <AppRouter />
                    <script
                      dangerouslySetInnerHTML={{
                        __html: `
                        !function(f,b,e,v,n,t,s)
                        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                        n.queue=[];t=b.createElement(e);t.async=!0;
                        t.src=v;s=b.getElementsByTagName(e)[0];
                        s.parentNode.insertBefore(t,s)}(window, document,'script',
                        'https://connect.facebook.net/en_US/fbevents.js');
                        fbq('init', '115434242677238');
                        fbq('track', 'PageView');
                      `
                      }}
                    />
                    <noscript>
                      <img
                        height="1"
                        width="1"
                        style="display:none"
                        src="https://www.facebook.com/tr?id=115434242677238&ev=PageView&noscript=1"
                      />
                    </noscript>
                  </>
                </Router>
              );
            }
          }}
        </AppConsumer>
      </AppProvider>
    );
  }
}

App.contextType = AppContext;

export default App;
