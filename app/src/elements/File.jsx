import React from 'react';

import { withFormsy } from 'formsy-react';

class File extends React.Component {
  static defaultProps = {
    className: 'form__field',
    inputClassName: 'form__input',
    labelClassName: 'form__placeholder',
    errorClassName: 'form__error',
    overlayClassName: 'form__overlay',
    iconClassName: 'form__icon',
    fileNameClassName: 'form__filename',
    accept: 'image/*',
    type: 'file',
  };

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      isFocused: false,
      isDirty: false,
      fileName: '',
      filePath: '',
      transitionDuration: 350,
    };

    this.changeValue = this.changeValue.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleFocus(e) {
    this.setState({
      isDirty: true,
      isFocused: true,
    });
  }

  handleBlur(e) {
    if (e.currentTarget.files.length) this.changeValue(e);
    this.setState({
      isDirty: true,
      isFocused: false,
    });
  }

  changeValue(e) {
    // setValue() will set the value of the component, which in
    // turn will validate it and the rest of the form
    // Important: Don't skip this step. This pattern is required
    // for Formsy to work.

    this.props.setValue(e.currentTarget.files[0]);
    this.setState({
      filePath: e.currentTarget.value || '',
      fileName: e.currentTarget.files[0].name,
      hasValue: e.currentTarget.files.length > 0,
    });
  }

  render() {
    // An error message is returned only if the component is invalid
    const errorMessage = this.props.getErrorMessage();
    const stateClassList = `${this.state.isFocused ? ' is-focused' : ''}${
      this.state.hasValue ? ' has-value' : ' no-value'
    }${this.state.hasError ? ' has-error' : ''}`;

    const classBuilder = (baseClass) => {
      let modClass = this.props.modifier ? ` ${baseClass}--${this.props.modifier}` : '';
      return `${baseClass} ${baseClass}--${this.props.type}${modClass}${stateClassList}`;
    };

    const showError =
      !this.props.isValid() && (this.props.isFormSubmitted() || !this.props.isPristine());

    return (
      <label className={classBuilder(this.props.className)}>
        {this.props.label && (
          <span className={classBuilder(this.props.labelClassName)}>{this.props.label}</span>
        )}
        <input
          className={classBuilder(this.props.inputClassName)}
          ref="$input"
          accept={this.props.accept}
          onChange={this.changeValue}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          type={this.props.type}
          value={this.state.filePath}
        />
        <i className={classBuilder(this.props.iconClassName)} />
        <span className={classBuilder(this.props.fileNameClassName)}>{this.state.fileName}</span>
        {showError && (
          <span className={classBuilder(this.props.errorClassName)}>{errorMessage}</span>
        )}
      </label>
    );
  }
}

export default withFormsy(File);
