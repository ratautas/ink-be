import { withFormsy } from 'formsy-react';
import React from 'react';

class Checker extends React.Component {
  static defaultProps = {
    className: 'form__field',
    inputClassName: 'form__native',
    controlClassName: 'form__control',
    labelClassName: 'form__inlabel',
    errorClassName: 'form__error',
    overlayClassName: 'form__overlay',
    type: 'checkbox',
    checked: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      isFocused: false,
      isDirty: false,
      isChecked: Boolean(props.getValue()),
    };

    this.changeValue = this.changeValue.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleFocus(e) {
    this.setState({
      isDirty: true,
      isFocused: true,
    });
  }

  handleBlur(e) {
    // this.changeValue(e);
    this.setState({
      isDirty: true,
      isFocused: false,
    });
  }

  changeValue(e) {
    const value = e.currentTarget.checked;
    this.setState({
      hasValue: value,
      isChecked: !this.state.isChecked,
    });
    // setValue() will set the value of the component, which in
    // turn will validate it and the rest of the form
    // Important: Don't skip this step. This pattern is required
    // for Formsy to work.
    this.props.setValue(value);
  }

  render() {
    // An error message is returned only if the component is invalid
    const errorMessage = this.props.getErrorMessage();
    const stateClassList = `${this.state.isFocused ? ' is-focused' : ''}${
      this.state.hasValue ? ' has-value' : ' no-value'
    }${this.state.hasError ? ' has-error' : ''}`;

    const classBuilder = (baseClass) => {
      let modClass = this.props.modifier ? ` ${baseClass}--${this.props.modifier}` : '';
      return `${baseClass} ${baseClass}--${this.props.type}${modClass}${stateClassList}`;
    };

    const showError =
      !this.props.isValid() && (this.props.isFormSubmitted() || !this.props.isPristine());

    return (
      <label className={classBuilder(this.props.className)}>
        <i className={classBuilder(this.props.controlClassName)} />
        <input
          className={classBuilder(this.props.inputClassName)}
          type={this.props.type}
          checked={this.state.isChecked}
          onChange={this.changeValue}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
        />
        {this.props.children && (
          <span className={classBuilder(this.props.labelClassName)}>{this.props.children}</span>
        )}
        {this.props.hasError && (
          <span className={classBuilder(this.props.errorClassName)}>{this.props.errorText}</span>
        )}
        {showError && (
          <span className={classBuilder(this.props.errorClassName)}>{errorMessage}</span>
        )}
      </label>
    );
  }
}

export default withFormsy(Checker);
