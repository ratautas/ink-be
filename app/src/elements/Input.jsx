import { withFormsy } from 'formsy-react';
import React from 'react';

class Input extends React.Component {
  static defaultProps = {
    className: 'form__field',
    inputClassName: 'form__input',
    labelClassName: 'form__placeholder',
    errorClassName: 'form__error',
    overlayClassName: 'form__overlay',
    type: 'text',
  };

  constructor(props) {
    super(props);

    this.state = {
      hasValue: false,
      hasError: false,
      isFocused: false,
      isDirty: false,
      transitionDuration: 350,
    };

    this.changeValue = this.changeValue.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleFocus(e) {
    this.setState({
      isDirty: true,
      isFocused: true,
    });
  }

  handleBlur(e) {
    this.changeValue(e);
    this.setState({
      isDirty: true,
      isFocused: false,
    });
  }

  changeValue(e) {
    // setValue() will set the value of the component, which in
    // turn will validate it and the rest of the form
    // Important: Don't skip this step. This pattern is required
    // for Formsy to work.
    const hasError = !this.props.isValid();
    this.props.setValue(e.currentTarget.value);
    this.setState({
      hasError,
      hasValue: e.currentTarget.value.length > 0,
    });
  }

  render() {
    // An error message is returned only if the component is invalid
    const errorMessage = this.props.getErrorMessage();

    const showError =
      !this.props.isValid() && (this.props.isFormSubmitted() || !this.props.isPristine());

    const stateClassList = `${this.state.isFocused ? ' is-focused' : ''}${
      this.state.hasValue ? ' has-value' : ' no-value'
    }${showError ? ' has-error' : ''}`;

    const classBuilder = (baseClass) => {
      let modClass = this.props.modifier ? ` ${baseClass}--${this.props.modifier}` : '';
      return `${baseClass} ${baseClass}--${this.props.type}${modClass}${stateClassList}`;
    };

    return (
      <label className={classBuilder(this.props.className)}>
        {this.props.label && (
          <span className={classBuilder(this.props.labelClassName)}>{this.props.label}</span>
        )}
        <input
          className={classBuilder(this.props.inputClassName)}
          onChange={this.changeValue}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          type={this.props.type}
          value={this.props.getValue() || ''}
        />
        {showError && (
          <span className={classBuilder(this.props.errorClassName)}>{errorMessage}</span>
        )}
      </label>
    );
  }
}

export default withFormsy(Input);
