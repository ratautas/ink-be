import React from 'react';

function Picture(props) {
  const { url, size, alt, classNames } = props;
  const mobileSize = 375;
  const mobileMedia = `(max-width: ${mobileSize}px)`;

  const api = 'https://cdn.shortpixel.ai/client/q_loseless,ret_wait';
  // const api = 'https://cdn.shortpixel.ai/client/q_glossy,ret_wait';
  const apiFullsize = `${api},w_${size || 800}`;
  const apiMobile = mobileSize > size ? apiFullsize : `${api},w_${mobileSize}`;

  const type = 'image/' + url.match(/\.[0-9a-z]{1,5}$/i)[0].replace('.', '');

  return (
    <picture>
      <source srcSet={`${apiMobile},to_webp/${url}`} type="image/webp" media={mobileMedia} />
      <source srcSet={`${apiMobile}/${url}`} type={type} media={mobileMedia} />
      <source srcSet={`${apiFullsize},to_webp/${url}`} type="image/webp" />
      <img alt={alt} className={classNames} loading="lazy" src={`${apiFullsize}/${url}`} />
    </picture>
  );
}

export default Picture;
