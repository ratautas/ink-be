import React from 'react';

import { TweenLite, CSSPlugin, AttrPlugin, Linear } from 'gsap/all';

// prevent tree shaking
export { CSSPlugin, AttrPlugin };

class Progress extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      size: 0,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.formStage !== this.props.formStage) {
      const d = this.props.duration / 1000;
      const { size, strokeWidth } = this.props;
      const r = (size - strokeWidth) / 2;
      const C = 2 * Math.PI * r;
      if (this.props.formStage === 1 && this.$stroke) {
        TweenLite.fromTo(
          this.$stroke,
          d * 9,
          { strokeDashoffset: C },
          {
            delay: d,
            strokeDashoffset: -C,
            ease: Linear.easeNone,
            repeat: -1,
          },
        );
      } else if (this.props.formStage === 2 && this.$stroke) {
        TweenLite.to(this.$stroke, d, { strokeDashoffset: 0 });
      }
    }
  }

  componentDidMount() {
    const d = this.props.duration / 1000;
    const { size, strokeWidth } = this.props;
    const r = (size - strokeWidth) / 2;
    const C = 2 * Math.PI * r;
    if (this.props.formStage === 1 && this.$stroke) {
      TweenLite.fromTo(
        this.$stroke,
        d * 9,
        { strokeDashoffset: C },
        {
          delay: d,
          strokeDashoffset: -C,
          ease: Linear.easeNone,
          repeat: -1,
        },
      );
    } else if (this.props.formStage === 2 && this.$stroke) {
      TweenLite.to(this.$stroke, d, { strokeDashoffset: 0 });
    }
  }

  render() {
    const size = this.props.size;
    const strokeWidth = this.props.strokeWidth;
    const r = (size - strokeWidth) / 2;
    const C = 2 * Math.PI * r;

    return (
      <svg
        ref={($el) => (this.$svg = $el)}
        width={size}
        height={size}
        viewBox={`0 0 ${size} ${size}`}>
        <circle
          ref={($el) => (this.$base = $el)}
          fill="none"
          cx={size / 2}
          cy={size / 2}
          r={r}
          stroke={this.props.base}
          strokeWidth={strokeWidth}
        />
        <circle
          ref={($el) => (this.$stroke = $el)}
          transform={`rotate(-90 ${size / 2} ${size / 2})`}
          fill="none"
          cx={size / 2}
          cy={size / 2}
          r={r}
          stroke={this.props.stroke}
          strokeWidth={strokeWidth}
          strokeDasharray={C}
          strokeDashoffset={C}
        />
      </svg>
    );
  }
}

export default Progress;
