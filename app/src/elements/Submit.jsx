import React from 'react';
import { CSSTransition } from 'react-transition-group';

import { AppContext } from '../AppContext';

import Progress from '../elements/Progress';

import hasAny from '../utils/hasAny';

class Submit extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      btnWidth: null,
      btnHeight: null,
    };
  }

  componentDidMount() {
    if (this.$btn) this.setState({ btnWidth: this.$btn.offsetWidth + 6 });
    if (this.$btn) this.setState({ btnHeight: this.$btn.offsetHeight });
  }

  render() {
    const formOptions =
      hasAny(this.context.options) && hasAny(this.context.options.form)
        ? this.context.options.form
        : null;

    return (
      <button
        className={`btn ${this.props.className} btn--submit btn--stage${this.props.formStage}`}
        ref={($el) => (this.$btn = $el)}
        style={{
          minWidth: this.state.btnWidth,
          minHeight: this.state.btnHeight,
        }}>
        <CSSTransition
          in={this.props.formStage === 0}
          mountOnEnter={true}
          unmountOnExit={true}
          classNames="btn__submit-"
          timeout={this.props.duration}>
          <span className="btn__submit">
            <span className="btn__text" ref={($el) => (this.$text0 = $el)}>
              {formOptions.send}
            </span>
            <i className="btn__arrow arrow" ref={($el) => (this.$icon0 = $el)} />
          </span>
        </CSSTransition>
        <CSSTransition
          in={this.props.formStage === 1 || this.props.formStage === 2}
          mountOnEnter={true}
          unmountOnExit={true}
          classNames="btn__submit-"
          timeout={this.props.duration}>
          <span className="btn__submit">
            <i className="submit__progress">
              <Progress
                ref={($el) => (this.$icon1 = $el)}
                formStage={this.props.formStage}
                base="#F1F2F2"
                stroke="#012664"
                duration={this.props.duration}
                size={24}
                strokeWidth={2}
              />
            </i>
          </span>
        </CSSTransition>
        <CSSTransition
          in={this.props.formStage === 3}
          mountOnEnter={true}
          unmountOnExit={true}
          classNames="btn__submit-"
          timeout={this.props.duration}>
          <span className="btn__submit">
            <i className="btn__icon btn__icon--success" />
            <span className="btn__text">Išsiųsta</span>
          </span>
        </CSSTransition>
        <CSSTransition
          in={this.props.formStage === 4}
          mountOnEnter={true}
          unmountOnExit={true}
          classNames="btn--progress"
          timeout={this.props.duration}>
          <span className="btn__submit">
            <i className="btn__icon btn__icon--error" />
            <span className="btn__text">Nepavyko</span>
          </span>
        </CSSTransition>
      </button>
    );
  }
}

Submit.contextType = AppContext;

export default Submit;
