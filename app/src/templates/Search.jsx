import React from 'react';
import { NavLink } from 'react-router-dom';

import * as latinize from 'latinize';

import { AppContext } from '../AppContext';

import { ReactComponent as IconSearch } from '../assets/img/icon--search.svg';

import hasAny from '../utils/hasAny';

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchData: [],
      query: '',
      renderLimit: 5,
      hasValue: false,
      hasError: false,
      isFocused: false,
      isDirty: false,
      results: [],
    };

    this.selectSuggestion = this.selectSuggestion.bind(this);
    this.changeValue = this.changeValue.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleFocus(e) {
    this.setState({ isFocused: true });
  }

  handleBlur(e) {
    this.setState({
      isDirty: true,
      isFocused: false,
    });
  }

  changeValue(query) {
    const renderLimit = Number(this.props.acf.render_limit) || 5;
    const hasValue = query.length > 0;
    const results = hasValue
      ? this.context.searchData
          .filter(item => {
            return (
              item.content.search(
                latinize(query)
                  .toLowerCase()
                  .trim(),
              ) > -1
            );
          })
          .filter((filtered, f) => {
            return f < renderLimit;
          })
      : [];

    this.setState({ query, hasValue, results, isDirty: true });
  }

  selectSuggestion(query) {
    this.changeValue(query);
  }

  render() {
    const stateClassList = `${this.state.isFocused ? ' is-focused' : ''}${
      this.state.hasValue ? ' has-value' : ' no-value'
    }`;

    const classBuilder = baseClass => {
      return `${baseClass} ${baseClass}--text ${baseClass}--white${stateClassList}`;
    };

    return (
      <div className="app__page app__page--search page" ref={$el => (this.$page = $el)}>
        <div className="page__search search">
          <div className="search__form form">
            <div className="search__container container container--s">
              <div className="search__row">
                <label className={classBuilder('form__field')}>
                  <span className={classBuilder('form__placeholder')}>
                    {this.props.title.rendered}
                  </span>
                  <input
                    className={classBuilder('form__input')}
                    onChange={e => this.changeValue(e.currentTarget.value)}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    type="text"
                    value={this.state.query}
                  />
                  <i className="search__icon">
                    <IconSearch />
                  </i>
                </label>
                {this.state.hasValue && (
                  <div className="search__results">
                    {this.state.results.length > 0 && (
                      <div className="search__list">
                        {this.state.results.map((result, r) => {
                          return (
                            <NavLink className="search__result" key={r} to={result.link}>
                              {result.title}
                            </NavLink>
                          );
                        })}
                      </div>
                    )}
                    {this.state.results.length === 0 && this.state.isDirty && (
                      <div className="search__no-results">{this.props.acf.no_results}</div>
                    )}
                  </div>
                )}
              </div>
            </div>
            <div className="search__suggestions">
              <div className="search__container container container--s">
                <div className="search__label">{this.props.acf.label}</div>
                {hasAny(this.props.acf) &&
                  hasAny(this.props.acf.list) &&
                  this.props.acf.list.map((item, i) => {
                    return (
                      <div
                        className="search__suggestion"
                        key={i}
                        onClick={() => this.selectSuggestion(item.item)}>
                        {item.item}
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Search.contextType = AppContext;

export default Search;
