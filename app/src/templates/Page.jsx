import React from 'react';

import Header from '../partials/Header';

import hasAny from '../utils/hasAny';

class Page extends React.Component {
  render() {
    const f = this.props.acf;
    return (
      <div className="app__page app__page--basic page" ref={($el) => (this.$page = $el)}>
        <Header
          ref={($el) => (this.$header = $el)}
          className="page__header"
          modifier={this.props.mod}
          header={{
            title: this.props.title ? this.props.title.rendered : '',
            intro: this.props.content ? this.props.content.rendered : '',
          }}
        />
        <main className="page__basic basic">
          <div className="basic__container container container--s">
            {hasAny(f.service_image) && (
              <img src={f.service_image} alt="" className="basic__image" />
            )}
            {hasAny(f.sections) && (
              <div className="basic__sections">
                {f.sections.map((section, s) => {
                  return (
                    <section className="basic__section" key={s}>
                      <div className="basic__content">
                        <div className="basic__subtitle">
                          <h3>{section.title}</h3>
                        </div>
                        <div
                          className="basic__text _wysiwyg"
                          dangerouslySetInnerHTML={{ __html: section.content }}
                        />
                      </div>
                    </section>
                  );
                })}
              </div>
            )}
          </div>
        </main>
      </div>
    );
  }
}

export default Page;
