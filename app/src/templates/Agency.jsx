import React from 'react';

import VisibilitySensor from 'react-visibility-sensor';
import { TweenLite, CSSPlugin, Power2 } from 'gsap/all';

import { AppContext } from '../AppContext';

import Header from '../partials/Header';
import Footer from '../partials/Footer';

import Bignow from '../components/Bignow';
import Clients from '../components/Clients';
import Geo from '../components/Geo';
import Features from '../components/Features';
import Services from '../components/Services';
import Members from '../components/Members';
import Modal from '../components/Modal';

import Picture from '../elements/Picture';

import unStyle from '../utils/unStyle';
import hasAny from '../utils/hasAny';

// prevent tree shaking
export { CSSPlugin };

class Agency extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // sameLocation: props.location.pathname === props.prevLocation,
      loadMore: 1,
      duration: 1200,
      mediaVisible: false,
      featuresVisible: false,
      servicesVisible: true,
      aboutVisible: true,
      bignowVisible: true,
    };
  }

  componentDidMount() {
    ////// timeout after page transition to reset states
    setTimeout(() => {
      this.setState({
        servicesVisible: false,
        aboutVisible: false,
        bignowVisible: false,
      });
    }, 0);
    // if (!this.props.sameLocation) {
    if (this.$services) TweenLite.set(this.$services, { opacity: 0.001 });
    if (this.Features && this.Features.$features) {
      TweenLite.set(this.Features.$features, { opacity: 0.001 });
    }
    if (this.$bignow) TweenLite.set(this.$bignow, { opacity: 0.001 });
    if (this.$about) TweenLite.set(this.$about, { opacity: 0.001 });
    if (this.$bignow) TweenLite.set(this.$bignow, { opacity: 0.001 });
    // }
  }

  render() {
    const handleMediaChange = isVisible => {
      // if (!this.state.mediaVisible && !this.state.sameLocation && isVisible) {
      if (!this.state.mediaVisible && isVisible) {
        this.setState({ mediaVisible: true });
        TweenLite.fromTo(
          this.$media,
          1.8,
          { x: window.innerWidth * -1.5 },
          {
            x: 0,
            delay: 0.5,
            ease: Power2.easeOut,
            onComplete: () => unStyle(this.$media),
          },
        );
      }
    };
    const handleFeaturesChange = isVisible => {
      if (!this.state.featuresVisible && isVisible && this.Features) {
        this.setState({ featuresVisible: true });
        TweenLite.set(this.Features.$features, { opacity: 1 });
        TweenLite.fromTo(
          this.Features.$features,
          1.8,
          { x: window.innerWidth * -1.5 },
          {
            x: 0,
            delay: 0.5,
            ease: Power2.easeOut,
            onComplete: () => {
              if (this.Features) unStyle(this.Features.$features);
            },
          },
        );
      }
    };
    const handleServicesChange = isVisible => {
      // if (!this.state.servicesVisible && !this.state.sameLocation && isVisible) {
      if (!this.state.servicesVisible && isVisible) {
        this.setState({ servicesVisible: true });
        TweenLite.set(this.$services, { opacity: 1 });
        TweenLite.fromTo(
          this.$services,
          1.5,
          { x: window.innerWidth * 1.5 },
          {
            x: 0,
            ease: Power2.easeOut,
            onComplete: () => unStyle(this.$services),
          },
        );
      }
    };
    const handleAboutChange = isVisible => {
      // if (!this.state.aboutVisible && !this.state.sameLocation && isVisible) {
      if (!this.state.aboutVisible && isVisible) {
        this.setState({ aboutVisible: true });
        TweenLite.set(this.$about, { opacity: 1 });
        TweenLite.fromTo(
          this.$about,
          1,
          { x: window.innerWidth * 0.5 },
          {
            x: 0,
            ease: Power2.easeOut,
            onComplete: () => unStyle(this.$about),
          },
        );
      }
    };
    const handleBignowChange = isVisible => {
      // if (!this.state.bignowVisible && !this.state.sameLocation && isVisible) {
      if (!this.state.bignowVisible && isVisible) {
        this.setState({ bignowVisible: true });
        TweenLite.set(this.$bignow, { opacity: 1 });
        TweenLite.fromTo(
          this.$bignow,
          1.5,
          { x: window.innerWidth * 1.5 },
          {
            x: 0,
            ease: Power2.easeOut,
            onComplete: () => unStyle(this.$bignow),
          },
        );
      }
    };

    const f = this.props.acf;

    const title = f.heading.reduce((title, head, h) => {
      return title + head.line + '<br>';
    }, '');

    return (
      <>
        <div className="app__page app__page--agency page" ref={$el => (this.$page = $el)}>
          <Header
            ref={$el => (this.$header = $el)}
            thisLocation={this.props.location.pathname}
            prevLocation={this.props.prevLocation}
            className="page__header"
            modifier="agency"
            header={{ title }}
          />

          <main className="page__agency agency">
            {f.hero && (
              <section className="agency__section agency__section--media">
                <VisibilitySensor
                  onChange={handleMediaChange}
                  partialVisibility={{ top: 0 }}
                  active={!this.state.mediaVisible}>
                  <div className="agency__media media" ref={$el => (this.$media = $el)}>
                    <Picture url={f.hero} classNames="media__image"  alt="INK agency" size={2000} />
                  </div>
                </VisibilitySensor>
              </section>
            )}

            {f.features && (
              <section className="agency__section agency__section--features">
                <div className="agency__container container agency__container--features">
                  <VisibilitySensor
                    onChange={change => handleFeaturesChange(change)}
                    partialVisibility={{ top: 0 }}
                    active={!this.state.featuresVisible}>
                    <Features
                      className="agency__features"
                      features={f.features}
                      ref={C => (this.Features = C)}
                    />
                  </VisibilitySensor>
                </div>
              </section>
            )}

            {hasAny(f.services) && (
              <VisibilitySensor
                onChange={handleServicesChange}
                partialVisibility={{ top: 0 }}
                active={!this.state.servicesVisible}>
                <section
                  className="agency__section agency__section--services"
                  ref={$el => (this.$services = $el)}>
                  <div className="agency__container container agency__container--services">
                    <Services services={f.services} className="agency__services" />
                  </div>
                </section>
              </VisibilitySensor>
            )}

            {hasAny(f.about) && (
              <section
                className="agency__section agency__section--about"
                ref={$el => (this.$about = $el)}>
                <div className="agency__container container agency__container--about">
                  <VisibilitySensor
                    onChange={handleAboutChange}
                    partialVisibility={{ top: 0 }}
                    active={!this.state.aboutVisible}>
                    <div className="agency__about about">
                      <div
                        className="about__intro _wysiwyg"
                        dangerouslySetInnerHTML={{ __html: f.about.intro }}
                      />
                      <div
                        className="about__text _wysiwyg"
                        dangerouslySetInnerHTML={{ __html: f.about.text }}
                      />
                    </div>
                  </VisibilitySensor>
                </div>
              </section>
            )}

            {hasAny(f.members) && (
              <section className="agency__section agency__section--members">
                <div className="agency__container container agency__container--members">
                  <Members {...f.members} ctx={this.context} className="agency__members" />
                </div>
              </section>
            )}

            {hasAny(f.clients) && (
              <section className="agency__section agency__section--clients">
                <div className="agency__container container agency__container--clients">
                  <Clients {...f.clients} className="agency__clients" />
                </div>
              </section>
            )}

            {hasAny(f.geo) && (
              <section className="agency__section agency__section--geo">
                <div className="agency__container container agency__container--geo">
                  <Geo {...f.geo} className="agency__geo" />
                </div>
              </section>
            )}

            {hasAny(f.bignow) && (
              <section
                className="agency__section agency__section--bignow"
                ref={$el => (this.$bignow = $el)}>
                <div className="agency__container container agency__container--bignow">
                  <VisibilitySensor
                    onChange={handleBignowChange}
                    partialVisibility={{ top: 0 }}
                    active={!this.state.bignowVisible}>
                    <Bignow {...f.bignow} className="agency__bignow" />
                  </VisibilitySensor>
                </div>
              </section>
            )}
          </main>
          <Footer className="page__footer" ref={$el => (this.$footer = $el)} />
        </div>
        {hasAny(f.services) &&
          f.services.list.map((service, s) => {
            return (
              <Modal
                key={s}
                title={service.service_title}
                text={service.service_text}
                image={service.service_image}
              />
            );
          })}
      </>
    );
  }
}

Agency.contextType = AppContext;

export default Agency;
