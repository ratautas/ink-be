import React from 'react';

import VisibilitySensor from 'react-visibility-sensor';

import { NavLink } from 'react-router-dom';

import { AppContext } from '../AppContext.jsx';

import Header from '../partials/Header';
import Footer from '../partials/Footer';

import Apply from '../components/Apply';

import hasAny from '../utils/hasAny';

class Career extends React.Component {
  render() {
    const f = this.props.acf;
    const positions = this.props.positions;
    return (
      <div className="app__page app__page--career page" ref={($el) => (this.$page = $el)}>
        <Header
          ref={($el) => (this.$header = $el)}
          thisLocation={this.props.location.pathname}
          prevLocation={this.props.prevLocation}
          className="page__header"
          modifier="career"
          header={{ lead: this.props.content.rendered }}
        />
        <main className="page__career career">
          <div className="career__container container">
            {f.media && (
              <div className="career__media media">
                <i className="media__image" style={{ backgroundImage: 'url(' + f.image + ')' }} />
              </div>
            )}
            {hasAny(this.props.positions) && (
              <div className="career__positions">
                {positions.map((position, p) => {
                  return (
                    <div className="career__position" key={p}>
                      <NavLink
                        className="career__trigger"
                        to={position.link.replace(document.body.dataset.root, '')}>
                        {position.title.rendered}
                      </NavLink>
                      <i className="career__arrow arrow" />
                    </div>
                  );
                })}
              </div>
            )}

            {hasAny(f.form) && (
              <Apply className="career__apply" position={this.props.title.rendered} {...f.form} />
            )}

            {hasAny(f.stats) && (
              <div className="career__employees employees">
                <div className="employees__content">
                  {f.stats.map((stat, s) => {
                    const feat = stat.is_featured;
                    return (
                      <VisibilitySensor key={s}>
                        <div
                          className={`employees__item${feat ? ' employees__item--featured' : ''}`}
                          key={s}>
                          <div
                            className={`employees__value${
                              feat ? ' employees__value--featured' : ''
                            }`}>
                            {stat.value}
                          </div>
                          <div
                            className={`employees__label${
                              feat ? ' employees__label--featured' : ''
                            }`}>
                            {stat.label}
                          </div>
                        </div>
                      </VisibilitySensor>
                    );
                  })}
                </div>
              </div>
            )}
          </div>
        </main>
        <Footer className="page__footer" ref={($el) => (this.$footer = $el)} />
      </div>
    );
  }
}

Career.contextType = AppContext;

export default Career;
