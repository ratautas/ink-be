import React from 'react';

import * as GoogleMapsLoader from 'google-maps';

import * as pin from '../assets/img/pin@2x.png';

import Header from '../partials/Header';

import styles from '../utils/mapStyles';
import hasAll from '../utils/hasAll';
import hasAny from '../utils/hasAny.js';

class Contact extends React.Component {
  componentDidMount() {
    let mapOptions = {};

    if (hasAll(this.props.acf.map)) {
      mapOptions = {
        zoom: Number(this.props.acf.map.zoom),
        center: {
          lat: Number(this.props.acf.map.lat),
          lng: Number(this.props.acf.map.lng),
        },
      };

      GoogleMapsLoader['KEY'] = 'AIzaSyCIqmRyWTM_nDdgcV1N6jdoTn4pWw6fEJo';
      GoogleMapsLoader['VERSION'] = '3.34';
      GoogleMapsLoader.load((google) => {
        const map = new google.maps.Map(this.$map, {
          ...mapOptions,
          styles,
        });
        new google.maps.Marker({
          map,
          position: new google.maps.LatLng(mapOptions.center.lat, mapOptions.center.lng),
          animation: google.maps.Animation.DROP,
          icon: {
            url: pin,
            size: new google.maps.Size(160, 174),
            scaledSize: new google.maps.Size(80, 87),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(40, 87),
          },
          // icon: {
          //   path:
          //     'M0,0V80H32.9292L40,87.071,47.0708,80H80V0ZM17.4619,' +
          //     '13.567H22.689V33.454l-5.2271.001Zm4.6987,52.238a2.9391,' +
          //     '2.9391,0,0,1-4.1777-.001v.001a2.9542,2.9542,0,1,1,' +
          //     '4.1777-4.178,2.9356,2.9356,0,0,1,0,4.178Zm26.15-52.238h4.9922l7.3741,' +
          //     '11.585V13.567H65.61V33.454l-4.9922.001L53.2456,' +
          //     '21.8669V33.454H48.3105ZM60.1562,66.676l-4.7631-8.009a8.8222,8.8222' +
          //     ',0,0,1-.9151.13c-.3218.03-.6328.0439-.9458.0439v7.84h-5.22V46.793h5.2251v7.431a3.5947,' +
          //     '3.5947,0,0,0,1.7881-.433A4.4853,4.4853,0,0,0,56.644,52.28l3.253-5.487h5.6338l-3.312,' +
          //     '5.43c-.5.832-.9693,1.553-1.3921,2.1619s-.8321,1.1451-1.2178,1.612L66.1641,66.676Z',
          //   fillColor: '#ea3824',
          //   fillOpacity: 1,
          //   anchor: new google.maps.Point(40, 87),
          //   strokeWeight: 0,
          //   scale: 1,
          // },
        });
      });
    }
  }

  render() {
    const f = this.props.acf;
    return (
      <div className="app__page app__page--contact page" ref={($el) => (this.$page = $el)}>
        <Header
          ref={($el) => (this.$header = $el)}
          thisLocation={this.props.location.pathname}
          prevLocation={this.props.prevLocation}
          className="page__header"
          modifier="contact"
          header={{ lead: this.props.content.rendered }}
        />
        <main className="page__contact contact">
          <div className="contact__container container">
            <div className="contact__details">
              <div className="contact__content">
                {f.company && <div className="contact__company">{f.company}</div>}
                {hasAny(f.contacts) && (
                  <div className="contact__addresses">
                    {f.contacts.map((contact, c) => {
                      return Boolean(contact.target) ? (
                        <a href={contact.target} className="contact__address" key={c}>
                          {contact.text}
                        </a>
                      ) : (
                        <div className="contact__address" key={c}>
                          {contact.text}
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
            </div>
            <div className="contact__map" ref={($el) => (this.$map = $el)} />
          </div>
        </main>
      </div>
    );
  }
}

export default Contact;
