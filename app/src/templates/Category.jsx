import React from 'react';
import { NavLink } from 'react-router-dom';

import VisibilitySensor from 'react-visibility-sensor';
import { TweenLite, CSSPlugin } from 'gsap/all';
import { CustomEase } from '../vendor/gsap/CustomEase';

import { AppContext } from '../AppContext.jsx';

import Categories from '../partials/Categories';
import Footer from '../partials/Footer';
import Header from '../partials/Header';

import Author from '../components/Author';

import routeUrl from '../utils/routeUrl';
import unStyle from '../utils/unStyle';
import hasAny from '../utils/hasAny.js';

// prevent tree shaking
export { CSSPlugin };

class Category extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      renderLimit: 3,
      loadMore: 1,
      duration: 1200,
      posts: [],
    };

    this.getReadingTime = this.getReadingTime.bind(this);
  }

  componentDidMount() {
    const posts = this.context.posts
      .filter((post) => post.categories.indexOf(this.props.id) >= 0)
      .map((item, index) => {
        // return { ...item, visible: false };
        return { ...item, visible: this.props.location.pathname === this.context.prevPath };
      });
    const ctx = this.context;
    const miscOptions = hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;
    const renderLimit =
      hasAny(miscOptions) && hasAny(miscOptions.render_limit)
        ? Number(miscOptions.render_limit)
        : 3;
    const loadMore =
      hasAny(miscOptions) && hasAny(miscOptions.load_more) ? Number(miscOptions.load_more) : 1;
    this.setState({ posts, renderLimit, loadMore });
  }

  getReadingTime(post, speed) {
    let words = 0;
    const countWords = (str) => {
      return str.split(' ').length;
    };
    if (hasAny(post.acf)) {
      if (post.acf.intro) {
        words = post.acf.intro.lead ? countWords(post.acf.intro.lead) : 0;
        words = post.acf.intro.text ? words + countWords(post.acf.intro.text) : words;
      }
      if (hasAny(post.acf.sections)) {
        post.acf.sections.forEach((section) => {
          if (section.type === 'text') {
            words = section.title ? words + countWords(section.title) : words;
            words = section.text ? words + countWords(section.text) : words;
          }
        });
      }
    }
    return Math.round(words / speed);
  }

  render() {
    const ctx = this.context;
    const miscOptions = hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;

    const handlePostChange = (isVisible, index) => {
      const $elWrapper = this['$postWrapper' + index];
      const delay = index < 2 ? 0.8 : 0;
      const posts = this.state.posts;
      const x = index % 2 ? '-100%' : '100%';
      // const $elMedia = this['$postMedia' + index];
      // const width = window.innerWidth < 769 ? '100%' : '50%';
      if (!this.state.posts[index].visible) {
        if (isVisible) {
          posts[index].visible = true;
          this.setState({ posts });
          TweenLite.fromTo(
            $elWrapper,
            1.2,
            { x },
            {
              delay,
              x: '0%',
              ease: CustomEase.create('custom', '.48,0,.12,1'),
              onComplete: () => unStyle($elWrapper),
            },
          );
          // TweenLite.fromTo(
          //   $elMedia,
          //   1.5,
          //   { width: '40%' },
          //   {
          //     delay,
          //     width,
          //     ease: CustomEase.create('custom', '.48,0,.12,1'),
          //     onComplete: () => {
          //       unStyle($elMedia);
          //     },
          //   },
          // );
        } else {
          // TweenLite.set($elMedia, { width: '40%' });
          TweenLite.set($elWrapper, { x });
        }
      }
    };

    const splitHeading = (text) => {
      return text.split(' ').map((line, l) => (
        <span key={l} className={`line line--${l + 1}`}>
          {`${line}\xa0`}
        </span>
      ));
    };

    return (
      <>
        <div
          className="app__page app__page--listing app__page--blog page"
          ref={($el) => (this.$page = $el)}>
          <Header
            ref={($el) => (this.$header = $el)}
            thisLocation={this.props.location.pathname}
            prevLocation={this.props.prevLocation}
            className="page__header"
            modifier="blog"
            header={{ lead: this.props.name, text: this.props.description }}
          />

          {hasAny(ctx.categories) && <Categories className="page__categories" />}

          {this.state.posts.length > 0 ? (
            <main className="page__listing page__listing--blog listing">
              <div className="listing__sections listing__sections--blog">
                {this.state.posts
                  .filter((post, p) => p < this.state.renderLimit)
                  .map((post, p) => {
                    return (
                      <VisibilitySensor
                        onChange={(change) => handlePostChange(change, p)}
                        key={p}
                        partialVisibility={{ top: 0 }}>
                        <>
                          <section
                            className={`listing__section${
                              p === 0 ? ' listing__section--first' : ''
                            }`}>
                            <div
                              className={`listing__media listing__media--${
                                p % 2 ? 'left' : 'right'
                              } ${p === 0 ? 'listing__media--first' : ''} media`}
                              ref={($el) => (this['$postMedia' + p] = $el)}>
                              <NavLink
                                to={routeUrl(post.link)}
                                className="media__image"
                                style={{ backgroundImage: `url('${post.acf.image}')` }}
                              />
                            </div>
                            <div className="listing__container container container--l">
                              <div
                                className={`listing__content listing__content--blog listing__content--${
                                  p % 2 ? 'right' : 'left'
                                } ${p === 0 ? 'listing__content--first' : ''}`}>
                                <div
                                  className={`listing__wrapper listing__wrapper--blog listing__wrapper--${
                                    p % 2 ? 'right' : 'left'
                                  }`}
                                  ref={($el) => (this['$postWrapper' + p] = $el)}>
                                  <div className="listing__meta">
                                    {ctx.date(post.date)}
                                    {miscOptions && (
                                      <>
                                        <i className="dot" />
                                        {miscOptions.reading_speed
                                          ? this.getReadingTime(
                                              post,
                                              ctx.options.misc.reading_speed,
                                            )
                                          : null}
                                        {miscOptions.reading_label
                                          ? miscOptions.reading_label
                                          : null}
                                      </>
                                    )}
                                  </div>
                                  {post.categories && (
                                    <div className="listing__categories">
                                      {post.categories.map((id, c) => {
                                        const category = ctx.categoriesById[id];
                                        return (
                                          <NavLink
                                            key={c}
                                            to={routeUrl(category.link)}
                                            className="listing__category">
                                            {category.name}
                                          </NavLink>
                                        );
                                      })}
                                    </div>
                                  )}
                                  <NavLink className="listing__title" to={routeUrl(post.link)}>
                                    {/* <h2>{post.title.rendered}</h2> */}
                                    <h2>{splitHeading(post.title.rendered)}</h2>
                                  </NavLink>
                                  {post.member && (
                                    <Author
                                      author={post.member}
                                      className="listing__author"
                                      modifier="listing"
                                    />
                                  )}
                                </div>
                              </div>
                              <NavLink className="listing__link" to={routeUrl(post.link)} />
                            </div>
                          </section>
                        </>
                      </VisibilitySensor>
                    );
                  })}
              </div>
              {this.state.posts.length > this.state.renderLimit && miscOptions && miscOptions.more && (
                <div className="listing__footer">
                  <div
                    className="listing__more more"
                    onClick={() =>
                      this.setState({
                        renderLimit: this.state.renderLimit + this.state.loadMore,
                      })
                    }>
                    <>
                      <span className="more__label">{miscOptions.more}</span>
                      <i className="more__icon">
                        <span className="more__arrow arrow" />
                      </i>
                    </>
                  </div>
                </div>
              )}
            </main>
          ) : (
            <main className="page__listing page__listing--blog listing">
              <div className="listing__no-results">
                {hasAny(miscOptions) && hasAny(miscOptions.no_posts) && (
                  <h3>{miscOptions.no_posts}</h3>
                )}
              </div>
            </main>
          )}
          <Footer className="page__footer" modifier="blog" ref={($el) => (this.$footer = $el)} />
        </div>
        {/* {hasAny(ctx.categories) && <Categories className="app__categories" />} */}
      </>
    );
  }
}

Category.contextType = AppContext;

export default Category;
