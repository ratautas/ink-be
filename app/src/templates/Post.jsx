import React from "react";
import { NavLink } from "react-router-dom";

import { TweenLite, ScrollToPlugin, CSSPlugin, AttrPlugin } from "gsap/all";

import Swiper from "react-id-swiper";
import throttle from "lodash.throttle";
import shareThis from "share-this";
import * as twitterSharer from "share-this/dist/sharers/twitter";
import * as facebookSharer from "share-this/dist/sharers/facebook";
import * as linkedinSharer from "share-this/dist/sharers/linked-in";

import { AppContext } from "../AppContext.jsx";

import Share from "../partials/Share";
import Intro from "../partials/Intro";
import Footer from "../partials/Footer";

import Author from "../components/Author";
import Bio from "../components/Bio";
import Subscription from "../components/Subscription";

import routeUrl from "../utils/routeUrl";
import bg from "../utils/bg";
import hasAny from "../utils/hasAny.js";

// prevent tree shaking
export { ScrollToPlugin, CSSPlugin, AttrPlugin };

class Post extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      postHeight: 0,
      postOffset: 0,
      progress: 0,
      progressed: false,
      swiperParams: {
        speed: 800,
        slidesPerView: 2,
        loop: true,
        simulateTouch: false,
        navigation: {
          nextEl: ".latest__next.arrow",
          prevEl: ".latest__prev.arrow"
        },
        breakpoints: {
          767: {
            slidesPerView: 1
          }
        }
      }
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.handleScrollToTop = this.handleScrollToTop.bind(this);

    window.onscroll = throttle(this.handleScroll, 60);
  }

  handleScroll() {
    const progress = (window.pageYOffset / this.state.postHeight) * 100;
    if (!this.state.progressed) {
      if (progress > this.state.progress && progress < 100) {
        this.setState({ progress });
        if (this.$progress)
          TweenLite.set(this.$progress, { width: `${progress}%` });
      } else if (progress > 100 && progress < 120) {
        this.setState({ progress: 100, progressed: true });
        if (this.$progress) TweenLite.set(this.$progress, { width: "100%" });
      }
    }
  }

  handleScrollToTop() {
    TweenLite.to("html", 0.75, { scrollTo: 0 });
  }

  componentDidMount() {
    const postHeight =
      this.$pageIntro.clientHeight +
      this.$postIntro.clientHeight +
      this.$postSections.clientHeight -
      window.innerHeight / 2;

    const postOffset = this.$postIntro.getBoundingClientRect().top;

    this.setState({ postHeight, postOffset, progress: 0 });

    if (this.$progress) TweenLite.set(this.$progress, { width: 0 });

    shareThis({
      selector: ".post",
      sharers: [facebookSharer, twitterSharer, linkedinSharer]
    }).init();
  }

  componentWillUnmount() {
    this.setState({ progress: 0 });
    if (this.$progress) TweenLite.set(this.$progress, { width: 0 });
  }

  render() {
    const ctx = this.context;
    const post = this.props;
    const i = this.props.index;
    const prevPost =
      i !== 0 ? ctx.posts[i - 1] : ctx.posts[ctx.posts.length - 1];
    const nextPost =
      i !== ctx.posts.length - 1 ? ctx.posts[i + 1] : ctx.posts[0];
    const miscOptions =
      hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;

    const slider_posts = ctx.posts
      .filter(slide => slide.id !== post.id)
      .slice(0, 7);

    return (
      <>
        <div
          className="app__page app__page--post page"
          ref={$el => (this.$page = $el)}
        >
          <div ref={$el => (this.$pageIntro = $el)}>
            <Intro
              ref={$el => (this.$header = $el)}
              className="page__intro"
              {...post}
            />
          </div>
          <main className="page__post post">
            {post.acf.intro && (
              <div className="post__intro" ref={$el => (this.$postIntro = $el)}>
                <div className="post__container container container--s">
                  <div
                    className="post__lead _wysiwyg"
                    dangerouslySetInnerHTML={{ __html: post.acf.intro.lead }}
                  />
                  <div
                    className="post__text _wysiwyg"
                    dangerouslySetInnerHTML={{ __html: post.acf.intro.text }}
                  />
                </div>
              </div>
            )}
            <Share className="post__share" post={post} />

            {prevPost && (
              <NavLink
                to={routeUrl(prevPost.link)}
                className="post__nav post__nav--prev more"
              >
                <i className="more__icon">
                  <span className="more__arrow arrow" />
                </i>
              </NavLink>
            )}

            {nextPost && (
              <NavLink
                to={routeUrl(nextPost.link)}
                className="post__nav post__nav--next more"
              >
                <i className="more__icon">
                  <span className="more__arrow arrow" />
                </i>
              </NavLink>
            )}

            <div
              className="post__sections"
              ref={$el => (this.$postSections = $el)}
            >
              {post.acf.sections &&
                post.acf.sections.map((section, s) => {
                  return (
                    <section className="post__section" key={s}>
                      {section.type === "text" ? (
                        <div className="post__container container container--s">
                          <div className="post__subtitle">
                            <h3>{section.title}</h3>
                            <div
                              className="post__content _wysiwyg"
                              dangerouslySetInnerHTML={{ __html: section.text }}
                            />
                          </div>
                        </div>
                      ) : (
                        <div className="post__container container container--l">
                          <div className="post__media media">
                            <i
                              className="media__image"
                              style={bg(section.image)}
                            />
                          </div>
                        </div>
                      )}
                    </section>
                  );
                })}
              {hasAny(miscOptions) && miscOptions.to_top && (
                <div
                  className="post__totop totop"
                  onClick={this.handleScrollToTop}
                >
                  <span className="totop__label">{miscOptions.to_top}</span>
                  <i className="totop__arrow arrow" />
                </div>
              )}
              {post.member && <Bio {...post.member} className="post__bio" />}
            </div>
            <div className="post__latest latest">
              <div className="latest__container container">
                {hasAny(miscOptions) && miscOptions.latest_label && (
                  <div className="latest__title">
                    <h2>{miscOptions.latest_label}</h2>
                  </div>
                )}
                <div className="latest__content">
                  <Swiper {...this.state.swiperParams}>
                    {slider_posts.map((slide, s) => {
                      return (
                        <div className="latest__slide slide" key={s}>
                          <NavLink
                            to={routeUrl(slide.link)}
                            className="slide__media media"
                          >
                            <i
                              className="media__image"
                              style={bg(slide.acf.image)}
                            />
                          </NavLink>
                          <div className="slide__content">
                            <div className="slide__meta">
                              {ctx.date(slide.date)}
                            </div>
                            <NavLink
                              to={routeUrl(slide.link)}
                              className="slide__title"
                            >
                              {slide.title.rendered}
                            </NavLink>
                            {slide.member && (
                              <Author
                                author={slide.member}
                                className="slide__author"
                                modifier="slide"
                              />
                            )}
                          </div>
                        </div>
                      );
                    })}
                  </Swiper>
                </div>
              </div>
            </div>
            <Subscription className="post__subscription" />
          </main>
          <Footer className="page__footer" ref={$el => (this.$footer = $el)} />
        </div>
        <div className="app__progress" ref={$el => (this.$progress = $el)} />
      </>
    );
  }
}

Post.contextType = AppContext;

export default Post;
