import React from 'react';
import { NavLink } from 'react-router-dom';

import VisibilitySensor from 'react-visibility-sensor';
import { TweenLite, CSSPlugin } from 'gsap/all';
import { CustomEase } from '../vendor/gsap/CustomEase';

import { AppContext } from '../AppContext.jsx';

import Header from '../partials/Header';
import Footer from '../partials/Footer';

import routeUrl from '../utils/routeUrl';
import bg from '../utils/bg';
import unStyle from '../utils/unStyle';
import hasAny from '../utils/hasAny';

// prevent tree shaking
export {CSSPlugin};

class Cases extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      renderLimit: 3,
      loadMore: 2,
      cases: [],
    };
  }

  componentDidMount() {
    const cases = this.props.cases.map((item) => {
      return { ...item, visible: this.props.location.pathname === this.context.prevPath };
    });

    // console.log(cases);

    const ctx = this.context;
    const miscOptions = hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;
    const renderLimit =
      hasAny(miscOptions) && hasAny(miscOptions.render_limit)
        ? Number(miscOptions.render_limit)
        : 3;
    const loadMore =
      hasAny(miscOptions) && hasAny(miscOptions.load_more) ? Number(miscOptions.load_more) : 1;
    this.setState({ cases, renderLimit, loadMore });
  }

  getThumbnail(story) {
    return hasAny(story.acf.thumbnail) ? bg(story.acf.thumbnail) : bg(story.acf.image);
  }

  render() {
    const ctx = this.context;
    const miscOptions = hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;
    // const thumbnail = hasAny()

    const handleStoryChange = (isVisible, index) => {
      const $elWrapper = this['$storyWrapper' + index];
      const delay = index < 2 ? 0.8 : 0;
      const cases = this.state.cases;
      const x = index % 2 ? '-100%' : '100%';
      // const $elMedia = this['$storyMedia' + index];
      // const width = window.innerWidth < 769 ? '100%' : '50%';
      if (!this.state.cases[index].visible) {
        if (isVisible) {
          cases[index].visible = true;
          this.setState({ cases });
          TweenLite.fromTo(
            $elWrapper,
            1.2,
            { x },
            {
              delay,
              x: '0%',
              ease: CustomEase.create('custom', '.48,0,.12,1'),
              onComplete: () => unStyle($elWrapper),
            },
          );
          // TweenLite.fromTo(
          //   $elMedia,
          //   1.5,
          //   { width: '40%' },
          //   {
          //     delay,
          //     width,
          //     ease: CustomEase.create('custom', '.48,0,.12,1'),
          //     onComplete: () => unStyle($elMedia),
          //   },
          // );
        } else {
          // TweenLite.set($elMedia, { width: '40%' });
          TweenLite.set($elWrapper, { x });
        }
      }
    };

    return (
      <div className="app__page app__page--stories page" ref={($el) => (this.$page = $el)}>
        <Header
          ref={($el) => (this.$header = $el)}
          thisLocation={this.props.location.pathname}
          prevLocation={this.props.prevLocation}
          className="page__header"
          modifier="stories"
          header={{ lead: this.props.content.rendered }}
        />
        {this.state.cases.length > 0 && (
          <main className="page__listing page__listing--cases listing">
            <div className="listing__sections">
              {this.state.cases
                .filter((story, s) => s < this.state.renderLimit)
                .map((story, s) => {
                  return (
                    <VisibilitySensor
                      onChange={(change) => handleStoryChange(change, s)}
                      key={s}
                      partialVisibility={{ top: 0 }}>
                      <section
                        className={`listing__section${s === 0 ? ' listing__section--first' : ''}`}
                        ref={($el) => (this['$story' + s] = $el)}>
                        <div
                          className={`listing__media listing__media--${s % 2 ? 'left' : 'right'} ${
                            s === 0 ? 'listing__media--first' : ''
                          } media`}
                          ref={($el) => (this['$storyMedia' + s] = $el)}>
                          <NavLink
                            to={routeUrl(story.link)}
                            className="media__image"
                            style={this.getThumbnail(story)}
                          />
                        </div>
                        <div className="listing__container container container--l">
                          <div
                            className={`listing__content listing__content--${
                              s % 2 ? 'right' : 'left'
                            } ${s === 0 ? 'listing__content--first' : ''}`}>
                            <div
                              className={`listing__wrapper listing__wrapper--${
                                s % 2 ? 'right' : 'left'
                              }`}
                              ref={($el) => (this['$storyWrapper' + s] = $el)}>
                              {hasAny(story.acf.year) && (
                                <div className="listing__meta">{story.acf.year}</div>
                              )}
                              <NavLink
                                className="listing__title"
                                to={story.link.replace(document.body.dataset.root, '')}>
                                <h2>{`${story.acf.label ? story.acf.label + '. ' : ''}${
                                  story.title.rendered
                                }`}</h2>
                              </NavLink>
                            </div>
                          </div>
                        </div>
                        <NavLink className="listing__link" to={routeUrl(story.link)} />
                      </section>
                    </VisibilitySensor>
                  );
                })}
            </div>

            {this.props.cases.length > this.state.renderLimit && (
              <div className="listing__footer">
                <div
                  className="listing__more more"
                  onClick={() =>
                    this.setState({
                      renderLimit: this.state.renderLimit + this.state.loadMore,
                    })
                  }>
                  {hasAny(miscOptions) && miscOptions.more && (
                    <span className="more__label">{miscOptions.more}</span>
                  )}
                  <i className="more__icon">
                    <span className="more__arrow arrow" />
                  </i>
                </div>
              </div>
            )}
          </main>
        )}
        <Footer className="page__footer" ref={($el) => (this.$footer = $el)} />
      </div>
    );
  }
}

Cases.contextType = AppContext;

export default Cases;
