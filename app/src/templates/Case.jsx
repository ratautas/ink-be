import React from 'react';
import { NavLink } from 'react-router-dom';

import VisibilitySensor from 'react-visibility-sensor';
import { TweenLite, Linear } from 'gsap/all';

import { AppContext } from '../AppContext.jsx';

import Header from '../partials/Header';
import Footer from '../partials/Footer';

import routeUrl from '../utils/routeUrl';
import hasAny from '../utils/hasAny';

class Case extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      stats: [],
    };

    this.handleStatChange = this.handleStatChange.bind(this);
  }

  componentDidMount() {
    const stats =
      hasAny(this.props.acf) &&
      hasAny(this.props.acf.victories) &&
      hasAny(this.props.acf.victories.items)
        ? this.props.acf.victories.items.map((stat, s) => {
            const countTo = isNaN(Number(stat.value)) ? false : Number(stat.value);
            const value = countTo ? 0 : stat.value;
            return {
              ...stat,
              countTo,
              value,
              visible: this.props.location.pathname === this.context.prevPath,
            };
          })
        : [];
    this.setState({ stats });
  }

  handleStatChange = (isVisible, i) => {
    const stat = this.state.stats[i];
    if (!stat.visible && isVisible) {
      let stats = this.state.stats;
      stats[i].visible = true;
      this.setState({ stats });
      if (stat.countTo) {
        TweenLite.to(stat, 1, {
          value: stat.countTo,
          roundProps: 'value',
          ease: Linear.easeNone,
          onUpdate: () => {
            let stats = this.state.stats;
            stats[i].value = Math.floor(stat.value);
            this.setState({ stats });
          },
        });
      }
    }
  };

  render() {
    const ctx = this.context;
    const i = this.props.index;
    const f = this.props.acf;
    const prevCase = i !== 0 ? ctx.cases[i - 1] : ctx.cases[ctx.cases.length - 1]; // loop
    const nextCase = i !== ctx.cases.length - 1 ? ctx.cases[i + 1] : ctx.cases[0]; // loop
    const background = hasAny(f) && hasAny(f.background) ? f.background : null;

    const solutionsHtml =
      hasAny(f) && hasAny(f.solutions) && hasAny(f.solutions.text)
        ? f.solutions.text.replace(
            new RegExp('<li>', 'g'),
            `<li><i class="solutions__bullet" style="background-color:${background}"></i>`,
          )
        : '';

    window['hem'] = 'asd';

    return (
      <div className="app__page app__page--case page" ref={($el) => (this.$page = $el)}>
        {f.label && (
          <Header
            ref={($el) => (this.$header = $el)}
            thisLocation={this.props.location.pathname}
            prevLocation={this.props.prevLocation}
            className="page__header"
            modifier="case"
            header={{ label: f.label, title: this.props.title.rendered }}
          />
        )}
        {hasAny(f) && (
          <main className="page__case case">
            {f.image && (
              <section className="case__section case__section--media">
                {/* <div className="case__container case__container--media container"> */}
                <div className="case__container case__container--media">
                  <div className="case__media media">
                    <i className="media__image" style={{ backgroundImage: `url('${f.image}')` }} />
                  </div>
                </div>
              </section>
            )}
            {hasAny(f.task) && (
              <section className="case__section case__section--task">
                <div className="case__container case__container--task container">
                  <div
                    className="case__task task"
                    style={{
                      backgroundColor: background ? background : null,
                      color: f.color ? f.color : null,
                    }}>
                    <div className="task__content">
                      {f.task.title && (
                        <div className="task__title">
                          <h2>{f.task.title}</h2>
                        </div>
                      )}
                      {f.task.text && (
                        <div
                          className="task__text _wysiwyg"
                          dangerouslySetInnerHTML={{ __html: f.task.text }}
                        />
                      )}
                      {f.task.idea_title && (
                        <div className="task__title">
                          <h2>{f.task.idea_title}</h2>
                        </div>
                      )}
                      {f.task.idea_text && (
                        <div
                          className="task__text _wysiwyg"
                          dangerouslySetInnerHTML={{ __html: f.task.idea_text }}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </section>
            )}
            {hasAny(f.solutions) && (
              <section className="case__section case__section--solutions">
                <div className="case__container case__container--solutions container">
                  <div className="case__solutions solutions">
                    <div className="solutions__content">
                      {f.solutions.title && (
                        <div className="solutions__title">
                          <h2>{f.solutions.title}</h2>
                        </div>
                      )}
                      {f.solutions.text && (
                        <div
                          className="solutions__text _wysiwyg"
                          dangerouslySetInnerHTML={{ __html: solutionsHtml }}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </section>
            )}
            {hasAny(f.victories) && (
              <section className="case__section case__section--victories">
                <div className="case__container case__container--victories container">
                  <div className="case__victories victories">
                    <div className="victories__content">
                      {f.victories.title && (
                        <div className="victories__title">
                          <h2>{f.victories.title}</h2>
                        </div>
                      )}
                      {hasAny(this.state.stats) && (
                        <div className="victories__stats stats">
                          {this.state.stats.map((stat, s) => {
                            return (
                              <VisibilitySensor
                                onChange={(change) => this.handleStatChange(change, s)}
                                partialVisibility={{ top: 0 }}
                                key={s}>
                                <div className="stats__item" key={s}>
                                  <div className="stats__achievement">
                                    <span className="stats__prefix">{stat.prefix}</span>
                                    <span
                                      className="stats__value"
                                      ref={($el) => (this.$countTo = $el)}>
                                      {stat.value}
                                    </span>
                                    <span className="stats__suffix">{stat.suffix}</span>
                                  </div>
                                  <div className="stats__label">{stat.label}</div>
                                </div>
                              </VisibilitySensor>
                            );
                          })}
                        </div>
                      )}
                      {f.victories.source && (
                        <div className="victories__source">{f.victories.source}</div>
                      )}
                    </div>
                  </div>
                </div>
              </section>
            )}

            {hasAny(f.feedback) && (
              <section className="case__section case__section--feedback">
                <div className="case__container case__container--feedback container container--s">
                  <div className="case__feedback feedback">
                    <div className="feedback__content">
                      <div
                        className="feedback__text _wysiwyg"
                        dangerouslySetInnerHTML={{ __html: f.feedback.text }}
                      />
                      <div className="feedback__name">{f.feedback.name}</div>
                      <div className="feedback__position">{f.feedback.position}</div>
                    </div>
                  </div>
                </div>
              </section>
            )}

            {prevCase && nextCase && (
              <section className="case__section case__section--traverse">
                <div className="case__traverse traverse">
                  <div className="traverse__nav traverse__nav--prev more">
                    <i className="traverse__icon traverse__icon--prev more__icon">
                      <span className="more__arrow arrow" />
                    </i>
                    <div className="traverse__title traverse__title--prev">
                      {prevCase.title.rendered}
                    </div>
                    <div className="traverse__label traverse__label--prev">
                      {prevCase.acf.label}
                    </div>
                    <NavLink className="traverse__link" to={routeUrl(prevCase.link)} />
                  </div>
                  <NavLink to={routeUrl(ctx.casesPage.link)} className="traverse__grid grid">
                    <i className="grid__tile grid__tile--1" />
                    <i className="grid__tile grid__tile--2" />
                    <i className="grid__tile grid__tile--3" />
                    <i className="grid__tile grid__tile--4" />
                  </NavLink>
                  <div className="traverse__nav traverse__nav--next more">
                    <i className="traverse__icon traverse__icon--next more__icon">
                      <span className="more__arrow arrow" />
                    </i>
                    <div className="traverse__title traverse__title--next">
                      {nextCase.title.rendered}
                    </div>
                    <div className="traverse__label traverse__label--next">
                      {nextCase.acf.label}
                    </div>
                    <NavLink className="traverse__link" to={routeUrl(nextCase.link)} />
                  </div>
                </div>
              </section>
            )}
          </main>
        )}
        <Footer className="page__footer" ref={($el) => (this.$footer = $el)} />
      </div>
    );
  }
}

Case.contextType = AppContext;

export default Case;
