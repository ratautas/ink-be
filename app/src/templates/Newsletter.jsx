import React from 'react';

import Subscription from '../components/Subscription';

import hasAny from '../utils/hasAny.js';

class Newsletter extends React.Component {
  render() {
    const page = this.props.page;
    const title = hasAny(page) && hasAny(page.acf) && hasAny(page.acf.lead) ? page.acf.lead : null;

    return (
      <div className="app__page app__page--newsletter page" ref={($el) => (this.$page = $el)}>
        <div className="page__newsletter newsletter">
          <Subscription
            className="newsletter__subscription"
            title={title}
            text={page.content.rendered}
            modifier="white"
          />
        </div>
      </div>
    );
  }
}

export default Newsletter;
