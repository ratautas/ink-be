import React from 'react';

import Header from '../partials/Header';

import Apply from '../components/Apply';

import hasAny from '../utils/hasAny';

class Position extends React.Component {
  render() {
    const f = this.props.acf;
    return (
      <div className="app__page app__page--basic page" ref={($el) => (this.$page = $el)}>
        <Header
          ref={($el) => (this.$header = $el)}
          thisLocation={this.props.location.pathname}
          prevLocation={this.props.prevLocation}
          className="page__header"
          modifier="basic"
          header={{
            title: this.props.title.rendered,
            intro: this.props.content.rendered,
          }}
        />
        <main className="page__position position">
          <div className="basic__container container container--s">
            {hasAny(f.blocks) && (
              <div className="basic__sections">
                {f.blocks.map((block, b) => {
                  return (
                    <section className="basic__section" key={b}>
                      <div className="basic__content">
                        <div className="basic__subtitle">
                          <h3>{block.title}</h3>
                        </div>
                        <div
                          className={`basic__description _wysiwyg${
                            b % 2 ? ' basic__description--blue' : ''
                          }`}
                          dangerouslySetInnerHTML={{ __html: block.description }}
                        />
                      </div>
                    </section>
                  );
                })}
              </div>
            )}
            {/* {hasAny(f.form) && ( */}
            <Apply
              className="basic__apply"
              heading={true}
              position={this.props.title.rendered}
              {...f.form}
            />
            {/* )} */}
          </div>
        </main>
      </div>
    );
  }
}

export default Position;
