import React from 'react';
import { NavLink } from 'react-router-dom';

import { AppContext } from '../AppContext.jsx';

import Footer from '../partials/Footer';

class NotFound extends React.Component {
  render() {
    const homeUrl = this.context.lang === 'lt' ? '/' : '/' + this.context.lang;

    return (
      <div className="app__page app__page--notfound page" ref={($el) => (this.$page = $el)}>
        <main className="page__notfound notfound">
          <div className="notfound__container container container--s">
            <div className="notfound__content">
              <div className="notfound__title">
                <h1>{this.props.title}</h1>
              </div>
              <div
                className="notfound__text _wysiwyg"
                dangerouslySetInnerHTML={{ __html: this.props.text }}
              />
              <NavLink className="notfound__back" to={homeUrl}>
                <i className="notfound__arrow arrow" />
                <span className="notfound__label">{this.props.back}</span>
              </NavLink>
            </div>
          </div>
        </main>
        <Footer className="page__footer" modifier="blue" ref={($el) => (this.$footer = $el)} />
      </div>
    );
  }
}

NotFound.contextType = AppContext;

export default NotFound;
