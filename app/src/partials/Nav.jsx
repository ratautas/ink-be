import React from 'react';
import { NavLink } from 'react-router-dom';
import { Transition } from 'react-transition-group';

import { TweenLite, CSSPlugin, Power2 } from 'gsap/all';
import { CustomEase } from '../vendor/gsap/CustomEase';

import { AppContext } from '../AppContext.jsx';

import Langs from '../partials/Langs';

import unStyle from '../utils/unStyle.js';
import routeUrl from '../utils/routeUrl';

// prevent tree shaking
export { CSSPlugin };

class Nav extends React.Component {
  t(t) {
    return (t * this.context.tranSpeed) / 1000;
  }

  componentDidMount() {
    this.handleContentEnter = this.handleContentEnter.bind(this);
    this.handleContentExit = this.handleContentExit.bind(this);
  }

  handleContentEnter() {
    const t = (t) => (t * this.context.tranSpeed) / 1000;
    const $blue =
      window.innerWidth < 768 && this.Langs && this.Langs.$langs ? this.Langs.$langs : this.$blue;
    TweenLite.fromTo(
      this.$content,
      this.t(1.1),
      { x: window.innerWidth },
      {
        x: 0,
        delay: t(0.3),
        ease: Power2.easeOut,
        onComplete: () => {
          unStyle(this.$content);
        },
      },
    );

    TweenLite.fromTo(
      $blue,
      this.t(0.9),
      { x: window.innerWidth, scaleX: 0 },
      {
        x: 0,
        scaleX: 1,
        delay: t(0.3),
        ease: CustomEase.create('custom', '.48,0,.12,1'),
        onComplete: () => {
          unStyle($blue);
        },
      },
    );
  }

  handleContentExit(el) {
    const $blue =
      window.innerWidth < 768 && this.Langs && this.Langs.$langs ? this.Langs.$langs : this.$blue;
    TweenLite.to(this.$content, this.t(1.5), {
      x: window.innerWidth * -1.3,
      ease: CustomEase.create('custom', '.48,0,.12,1'),
    });
    TweenLite.to($blue, this.t(1.05), {
      x: -window.innerWidth,
      delay: this.t(0.25),
      ease: CustomEase.create('custom', '.48,0,.12,1'),
    });
  }

  render() {
    const ctx = this.context;
    // const baseUrl = ctx.rootUrl.replace(`/${ctx.lang}/`, '');
    // console.log(baseUrl);
    return (
      <Transition
        onEnter={this.handleContentEnter}
        onExit={this.handleContentExit}
        mountOnEnter={true}
        unmountOnExit={true}
        timeout={this.t(1600)}
        in={ctx.navOpen}>
        <nav className="app__nav nav">
          <div className="nav__content" ref={($el) => (this.$content = $el)}>
            <ul className="nav__list">
              {ctx.nav.map((item, i) => {
                return (
                  <li key={i} className="nav__item">
                    <NavLink to={routeUrl(item.url)} className="nav__trigger trigger">
                      {item.title}
                    </NavLink>
                  </li>
                );
              })}
            </ul>
          </div>
          <div className="nav__blue" ref={($el) => (this.$blue = $el)} />
          <Langs ref={($el) => (this.Langs = $el)} className="nav__langs" />
        </nav>
      </Transition>
    );
  }
}

Nav.contextType = AppContext;

export default Nav;
