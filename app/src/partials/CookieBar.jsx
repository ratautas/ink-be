import React from 'react';
// import { NavLink, withRouter } from 'react-router-dom';
import { AppConsumer } from '../AppContext.jsx';
import { ReactComponent as Cookies } from '../assets/img/cookies.svg';

const CookieBar = props => {
  const handleAccept = () => {
    props.emitAccept();
  };

  return (
    <AppConsumer>
      {ctx => {
        if (ctx.options.cookiebar) {
          return (
            <div className="app__cookiebar cookiebar">
              <div className="cookiebar__content">
                <i className="cookiebar__icon">
                  <Cookies />
                </i>
                <div className="cookiebar__text">
                  {ctx.options.cookiebar.text ? ctx.options.cookiebar.text : ''}
                </div>
              </div>
              <div className="cookiebar__accept btn" onClick={handleAccept}>
                <span className="btn__text">
                  {ctx.options.cookiebar.accept ? ctx.options.cookiebar.accept : ''}
                </span>
                <i className="btn__arrow arrow" />
              </div>
            </div>
          );
        }
      }}
    </AppConsumer>
  );
};

export default CookieBar;
