import React from 'react';
import { NavLink } from 'react-router-dom';

import { AppContext } from '../AppContext.jsx';

import Author from '../components/Author';

import modClass from '../utils/modClass';
import routeUrl from '../utils/routeUrl';
import bg from '../utils/bg';
import hasAny from '../utils/hasAny';

class Intro extends React.Component {
  render() {
    const ctx = this.context;
    const miscOptions = hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;

    const getReadingTime = (post, speed) => {
      const countWords = (str) => {
        return str.split(' ').length;
      };
      let words = 0;
      if (post.acf) {
        if (post.acf.intro) {
          words = post.acf.intro.lead ? countWords(post.acf.intro.lead) : 0;
          words = post.acf.intro.text ? words + countWords(post.acf.intro.text) : words;
        }
        if (post.acf.sections.length) {
          post.acf.sections.forEach((section) => {
            if (section.type === 'text') {
              words = section.title ? words + countWords(section.title) : words;
              words = section.text ? words + countWords(section.text) : words;
            }
          });
        }
      }
      return Math.round(words / speed);
    };
    return (
      <header
        ref={($el) => (this.$header = $el)}
        className={modClass('intro', this.props.modifier, this.props.className)}>
        <div className="intro__container container">
          <div className="intro__media media">
            <i className="media__image" style={bg(this.props.acf.image)} />
          </div>
          <div className="intro__content intro__content--post">
            <div className="intro__meta">
              {ctx.date(this.props.date)}
              {miscOptions && (
                <>
                  <i className="dot" />
                  {miscOptions.reading_speed
                    ? getReadingTime(this.props, miscOptions.reading_speed)
                    : null}
                  {miscOptions.reading_label ? miscOptions.reading_label : null}
                </>
              )}
            </div>
            {this.props.categories && ctx.categoriesById && (
              <div className="intro__categories intro__categories--post">
                {this.props.categories.map((id, c) => {
                  const category = ctx.categoriesById[id];
                  return (
                    category && (
                      <NavLink
                        key={c}
                        to={routeUrl(category.link)}
                        className="intro__category intro__category--post">
                        {category.name}
                      </NavLink>
                    )
                  );
                })}
              </div>
            )}
            <div className="intro__title intro__title--post">
              <h1>{this.props.title.rendered}</h1>
            </div>
          </div>

          {this.props.member && (
            <Author
              author={this.props.member}
              className="intro__author"
              modifier="intro"
              label={true}
            />
          )}
        </div>
      </header>
    );
  }
}

Intro.contextType = AppContext;

export default Intro;
