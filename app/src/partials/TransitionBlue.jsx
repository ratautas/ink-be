import React from 'react';
import { Transition } from 'react-transition-group';

import { AppContext } from '../AppContext.jsx';

import { TweenLite, CSSPlugin } from 'gsap/all';
import { CustomEase } from '../vendor/gsap/CustomEase';

// prevent tree shaking
export { CSSPlugin };

class TransitionBlue extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onBlueEnter = this.onBlueEnter.bind(this);
  }

  t(t) {
    return (t * this.context.tranSpeed) / 1000;
  }

  onBlueEnter($blue) {
    TweenLite.fromTo(
      this.$blueTransition,
      this.t(3.6),
      { x: '100%' },
      {
        x: '-130%',
        delay: this.t(0.4),
        ease: CustomEase.create('custom', '.48,0,.12,1'),
        onComplete: () => {},
      },
    );
  }

  render() {
    return (
      <Transition
        in={this.context.pageTransition}
        mountOnEnter={true}
        unmountOnExit={true}
        onEnter={this.onBlueEnter}
        // timeout={this.t(2200)}>
        timeout={this.t(4400)}>
        <i
          className="app__transition app__transition--blue"
          ref={(el) => (this.$blueTransition = el)}
        />
      </Transition>
    );
  }
}

TransitionBlue.contextType = AppContext;

export default TransitionBlue;
