import React from 'react';
import { NavLink } from 'react-router-dom';

import { AppContext } from '../AppContext.jsx';

import Swiper from 'react-id-swiper';

import { ReactComponent as IconSearch } from '../assets/img/icon--search.svg';

import routeUrl from '../utils/routeUrl';
import hasAny from '../utils/hasAny.js';

class Categories extends React.Component {
  t(t) {
    return (t * this.context.tranSpeed) / 1000;
  }

  componentDidMount() {
    this.$categories.style.display = 'none';
    setTimeout(() => (this.$categories.style.display = ''), this.t(1000));
  }

  render() {
    const swiperParams = {
      speed: 300,
      freeMode: true,
      slidesPerView: 'auto',
      simulateTouch: false,
      navigation: {
        nextEl: '.categories__next.arrow',
        prevEl: '.categories__prev.arrow',
      },
    };

    const ctx = this.context;
    const miscOptions = hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;
    const allCategories = hasAny(miscOptions) ? miscOptions.all_categories : '';

    return (
      <div
        ref={($el) => (this.$categories = $el)}
        className={`${this.props.className} categories${
          this.props.modifier ? ' categories--' + this.props.modifier : ''
        }`}>
        <div className="categories__container">
          <div className="categories__list">
            <Swiper {...swiperParams}>
              <div className="categories__item">
                <NavLink
                  exact
                  to={routeUrl(ctx.blogPage.link)}
                  className="categories__trigger trigger trigger--red">
                  {allCategories}
                </NavLink>
              </div>
              {this.context.categories.map((category, c) => {
                return (
                  <div className="categories__item" key={c}>
                    <NavLink
                      to={routeUrl(category.link)}
                      className="categories__trigger trigger trigger--red">
                      {category.name}
                    </NavLink>
                  </div>
                );
              })}
            </Swiper>
          </div>
        </div>
        {hasAny(this.context.searchPage) && (
          <NavLink to={routeUrl(this.context.searchPage.link)} className="categories__search">
            <IconSearch />
          </NavLink>
        )}
      </div>
    );
  }
}

Categories.contextType = AppContext;

export default Categories;
