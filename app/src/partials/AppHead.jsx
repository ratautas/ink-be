import React from "react";
import { Helmet } from "react-helmet";
import ReactGA from "react-ga";

import { AppContext } from "../AppContext.jsx";

import hasAny from "../utils/hasAny.js";

let path = window.location.pathname;

ReactGA.initialize("UA-41126194-1");

class AppHead extends React.PureComponent {
  componentDidMount() {
    setTimeout(() => {
      ReactGA.pageview(path);
    }, 0);
  }

  componentDidUpdate() {
    if (this.props.location.pathname !== path) {
      path = this.props.location.pathname;
      setTimeout(() => ReactGA.pageview(path), 0);
    }
  }

  render() {
    const ctx = this.context;
    let subtitle =
      hasAny(ctx.options) && hasAny(ctx.options.title) ? ctx.options.title : "";
    // first, assign title
    let title =
      hasAny(this.props.title) && hasAny(this.props.title.rendered)
        ? this.props.title.rendered
        : "";

    // if 404:
    title =
      hasAny(this.props.title) && !hasAny(this.props.title.rendered)
        ? this.props.title
        : title;

    // if category:
    title = this.props.name ? this.props.name : title;

    let description =
      hasAny(ctx.options) &&
      hasAny(ctx.options.description) &&
      ctx.options.description;

    return (
      <Helmet>
        {(hasAny(title) || hasAny(subtitle)) && (
          <title>{`${title} | ${subtitle}`}</title>
        )}
        }
        <meta property="og:type" content="article" />
        <meta property="og:title" content={title} />
        <meta name="twitter:title" content={title} />
        <meta name="description" content={description} />
        <meta property="og:description" content={description} />
        <meta name="twitter:description" content={description} />
        <meta property="og:image" content="LINK TO THE IMAGE FILE" />
        <meta name="twitter:image" content="LINK TO IMAGE" />
        <meta property="og:url" content={this.props.link} />
        <meta property="og:site_name" content={subtitle} />
      </Helmet>
    );
  }
}

AppHead.contextType = AppContext;

export default AppHead;
