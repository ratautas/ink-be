import React from 'react';

import { AppContext } from '../AppContext.jsx';

class NavIcon extends React.Component {
  render() {
    return (
      <div
        className={`app__navicon navicon${this.context.navOpen ? ' is-open' : ''}`}
        ref={($el) => (this.$navicon = $el)}
        onClick={() => this.context.set({ navOpen: !this.context.navOpen })}>
        <div className="navicon__dots" ref={($el) => (this.$dots = $el)}>
          <i className="navicon__dot navicon__dot--1" />
          <i className="navicon__dot navicon__dot--2" />
          <i className="navicon__dot navicon__dot--3" />
          <i className="navicon__dot navicon__dot--4" />
          <i className="navicon__dot navicon__dot--5" />
          <i className="navicon__dot navicon__dot--6" />
          <i className="navicon__dot navicon__dot--7" />
          <i className="navicon__dot navicon__dot--8" />
          <i className="navicon__dot navicon__dot--9" />
        </div>
      </div>
    );
  }
}

NavIcon.contextType = AppContext;

export default NavIcon;
