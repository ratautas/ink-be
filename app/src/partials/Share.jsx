import React from 'react';
import { ReactComponent as IconLinkedin } from '../assets/img/icon_linkedin.svg';
import { ReactComponent as IconTwitter } from '../assets/img/icon_twitter.svg';
import { ReactComponent as IconFacebook } from '../assets/img/icon_facebook.svg';

import { FacebookShareButton, TwitterShareButton, LinkedinShareButton } from 'react-share';

const Share = (props) => {
  const url = window.location.href;

  return (
    <div className={`${props.className ? props.className + ' ' : ''}share`}>
      <div className="share__items">
        <div className="share__item share__item--linkedin">
          <IconLinkedin />
          <LinkedinShareButton url={url} />
        </div>
        <div className="share__item share__item--twitter">
          <IconTwitter />
          <TwitterShareButton url={url} />
        </div>
        <div className="share__item share__item--facebook">
          <IconFacebook />
          <FacebookShareButton url={url} />
        </div>
      </div>
    </div>
  );
};

export default Share;
