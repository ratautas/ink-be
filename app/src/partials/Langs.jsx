import React from 'react';

import { AppContext } from '../AppContext.jsx';

class Langs extends React.Component {
  render() {
    const ctx = this.context;
    const baseUrl = ctx.rootUrl.replace(`/${ctx.lang}/`, '');
    return (
      // <div className="app__langs langs" ref={($el) => (this.$langs = $el)}>
      <div className={`${this.props.className} langs`} ref={($el) => (this.$langs = $el)}>
        <div className="langs__content">
          <div className="langs__list">
            <a
              href={baseUrl}
              className={`langs__item${
                ctx.lang === 'lt' ? ' langs__item--active' : ''
              } trigger trigger--blue`}>
              LT
            </a>
            <a
              href={baseUrl + '/en'}
              className={`langs__item${
                ctx.lang === 'en' ? ' langs__item--active' : ''
              } trigger trigger--blue`}>
              EN
            </a>
          </div>
        </div>
      </div>
    );
  }
}

Langs.contextType = AppContext;

export default Langs;
