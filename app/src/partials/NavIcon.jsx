import React from 'react';

import { AppContext } from '../AppContext.jsx';

import hasAny from '../utils/hasAny';

class NavIcon extends React.Component {
  render() {
    const miscOptions =
      hasAny(this.context.options) && hasAny(this.context.options.misc)
        ? this.context.options.misc
        : null;
    return (
      <div
        className={`app__navicon navicon${this.context.navOpen ? ' is-open' : ''}`}
        ref={($el) => (this.$navicon = $el)}
        onClick={() => this.context.set({ navOpen: !this.context.navOpen })}>
        {miscOptions && miscOptions.menu_label && (
          <div className="navicon__label">{miscOptions.menu_label}</div>
        )}
        <div className="navicon__lines">
          <i className="navicon__line navicon__line--1" />
          <i className="navicon__line navicon__line--2" />
          <i className="navicon__line navicon__line--3" />
        </div>
      </div>
    );
  }
}

NavIcon.contextType = AppContext;

export default NavIcon;
