import React from 'react';
import { NavLink } from 'react-router-dom';

import sanitizeHtml from 'sanitize-html';
import { TweenLite, CSSPlugin } from 'gsap/all';
import { CustomEase } from '../vendor/gsap/CustomEase';

import { AppContext } from '../AppContext.jsx';

import { ReactComponent as IconNavLinkedin } from '../assets/img/icon_linkedin.svg';

import modClass from '../utils/modClass';
import unStyle from '../utils/unStyle';
import bg from '../utils/bg';
import routeUrl from '../utils/routeUrl';

// prevent tree shaking
export { CSSPlugin };

class Header extends React.Component {
  t(t) {
    return (t * this.context.tranSpeed) / 1000;
  }

  componentDidMount() {
    if (this.context.prevPath !== this.props.thisLocation && this.$header) {
      const h = this.$header.clientHeight;
      TweenLite.set(this.$header, { y: -h * 2 });
      TweenLite.fromTo(
        this.$header,
        this.t(1.2),
        { y: -h * 2 },
        {
          y: 0,
          delay: this.t(1.6),
          ease: CustomEase.create('custom', '.48,0,.12,1'),
          onComplete: () => unStyle(this.$header),
        },
      );
      TweenLite.fromTo(
        this.$headerContent,
        this.t(1),
        { y: -h },
        {
          y: 0,
          delay: this.t(1.9),
          ease: CustomEase.create('custom', '.48,0,.12,1'),
          onComplete: () => unStyle(this.$headerContent),
        },
      );
    }
  }

  strip(string) {
    return sanitizeHtml(string, {
      allowedTags: ['b', 'i', 'em', 'strong', 'a', 'br'],
      allowedAttributes: {},
    });
  }

  render() {
    const header = this.props.header;
    const mod = this.props.modifier;
    if (header) {
      return (
        <header
          ref={$el => (this.$header = $el)}
          className={modClass('header', mod, this.props.className)}>
          <div className={modClass('header__container', mod, 'container')}>
            <div
              ref={$el => (this.$headerContent = $el)}
              className={modClass('header__content', mod)}>
              {header.label && (
                <div
                  className={modClass('_wysiwyg header__label', mod)}
                  dangerouslySetInnerHTML={{ __html: header.label }}
                />
              )}
              {header.title && (
                <div className={modClass('header__title', mod)}>
                  <h1 dangerouslySetInnerHTML={{ __html: this.strip(header.title) }} />
                </div>
              )}
              {header.lead &&
                (header.title ? (
                  <div
                    className={modClass('_wysiwyg header__lead', mod)}
                    dangerouslySetInnerHTML={{ __html: header.lead }}
                  />
                ) : (
                  <div className={modClass('header__lead', mod)}>
                    <h1 dangerouslySetInnerHTML={{ __html: this.strip(header.lead) }} />
                  </div>
                ))}
              {header.position && (
                <div className={modClass('header__position', mod)}>{header.position}</div>
              )}
              {header.intro &&
                (header.title || header.lead ? (
                  <div
                    className={modClass('_wysiwyg header__intro', mod)}
                    dangerouslySetInnerHTML={{ __html: header.intro }}
                  />
                ) : (
                  <div className={modClass('header__intro', mod)}>
                    <h1 dangerouslySetInnerHTML={{ __html: this.strip(header.intro) }} />
                  </div>
                ))}
              {header.text &&
                (header.title || header.lead || header.intro ? (
                  <div
                    className={modClass('_wysiwyg header__text', mod)}
                    dangerouslySetInnerHTML={{ __html: header.text }}
                  />
                ) : (
                  <div className={modClass('header__text', mod)}>
                    <h1>{this.strip(header.text)}</h1>
                  </div>
                ))}
              {header.image && (
                <div className={modClass('_wysiwyg header__image', mod)} style={bg(header.image)} />
              )}
              {header.action_label && header.action_page && (
                <NavLink
                  className="header__action btn btn--white"
                  to={routeUrl(header.action_page)}>
                  <span className="btn__text">{header.action_label}</span>
                  <i className="btn__arrow arrow" />
                </NavLink>
              )}
              {header.linkedin && (
                <a
                  className={modClass('header__linkedin', mod)}
                  target="_blank"
                  rel="noopener noreferrer"
                  href={header.linkedin}>
                  <IconNavLinkedin />
                </a>
              )}
            </div>
          </div>
        </header>
      );
    } else {
      return null;
    }
  }
}

Header.contextType = AppContext;

export default Header;
