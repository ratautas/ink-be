import React from 'react';
import { Transition, TransitionGroup } from 'react-transition-group';
import { Route, Switch } from 'react-router-dom';

import { TweenLite, CSSPlugin, Power2 } from 'gsap/all';
import { CustomEase } from '../vendor/gsap/CustomEase';

import { AppContext } from '../AppContext.jsx';

import Agency from '../templates/Agency';
import Blog from '../templates/Blog';
import Post from '../templates/Post';
import Page from '../templates/Page';
import Career from '../templates/Career';
import Position from '../templates/Position';
import Contact from '../templates/Contact';
import Cases from '../templates/Cases';
import Case from '../templates/Case';
import Member from '../templates/Member';
import Category from '../templates/Category';
import NotFound from '../templates/NotFound';
import Newsletter from '../templates/Newsletter';
import Search from '../templates/Search';

import unStyle from '../utils/unStyle';
import routeUrl from '../utils/routeUrl';
import hasAny from '../utils/hasAny.js';
import AppHead from './AppHead';

// prevent tree shaking
export { CSSPlugin };

class AppRouter extends React.Component {
  constructor(props) {
    super(props);
    this.onRouteEntered = this.onRouteEntered.bind(this);
    this.onRouteEnter = this.onRouteEnter.bind(this);
    this.onBlueEnter = this.onBlueEnter.bind(this);
    this.onRedEnter = this.onRedEnter.bind(this);
  }

  t(t) {
    return (t * this.context.tranSpeed) / 1000;
  }

  componentDidMount() {
    const path = this.switch.props.location.pathname;
    this.context.set({ prevPath: path });
  }

  onRouteEnter($route, location) {
    const path = location.pathname;
    if ($route) {
      if (path !== this.context.prevPath) {
        $route.style.display = 'none';
        this.context.set({ pageTransition: true });
      }
      this.context.set({
        samePath: Boolean(path === this.context.prevPath),
        thisPath: path,
        navOpen: false,
      });
    }
  }

  onRouteEntered($route, location) {
    const w = window.innerWidth;
    if ($route) {
      if (!this.context.samePath) {
        this.context.set({
          pageTransition: false,
          prevPath: location.pathname,
        });
        $route.style.display = '';
        window.scrollTo(0, 0);
        TweenLite.fromTo(
          $route,
          this.t(0.8),
          { x: w * 0.2 },
          {
            x: 0,
            delay: this.t(1),
            ease: Power2.easeOut,
            onComplete: () => unStyle($route),
          },
        );
      }
    }
  }

  onRouteExit($route, location) {
    const w = window.innerWidth;
    if (!this.context.samePath) {
      TweenLite.to($route, this.t(3.8), { x: w * -0.4, ease: Power2.easeOut });
    }
  }

  onBlueEnter($blue) {
    const w = window.innerWidth;
    if (!this.context.samePath) {
      TweenLite.fromTo(
        $blue,
        this.t(3.8),
        { x: w },
        {
          x: -w * 1.3,
          ease: CustomEase.create('custom', '.48,0,.12,1'),
        },
      );
    }
  }

  onRedEnter($red) {
    const w = window.innerWidth;
    if (!this.context.samePath) {
      TweenLite.fromTo(
        $red,
        this.t(4),
        { x: w },
        {
          x: -w * 1.3,
          ease: Power2.easeOut,
        },
      );
    }
  }

  render() {
    const ctx = this.context;
    return (
      <>
        {this.Route && <AppHead {...this.Route.props} />}
        <Transition
          in={this.context.pageTransition}
          mountOnEnter={true}
          unmountOnExit={true}
          onEnter={this.onBlueEnter}
          timeout={this.t(4400)}>
          <i className="app__transition app__transition--blue" />
        </Transition>
        <Transition
          in={this.context.pageTransition}
          mountOnEnter={true}
          unmountOnExit={true}
          onEnter={this.onRedEnter}
          timeout={this.t(2000)}>
          <i className="app__transition app__transition--red" />
        </Transition>
        <Route
          render={({ location }) => (
            <TransitionGroup className="app__router">
              <Transition
                key={location.key}
                timeout={{
                  enter: this.t(500),
                  exit: this.t(1000),
                }}
                onEnter={($route) => this.onRouteEnter($route, location)}
                onEntered={($route) => this.onRouteEntered($route, location)}
                onExit={($route) => this.onRouteExit($route, location)}
                mountOnEnter={true}
                unmountOnExit={true}>
                {(status) => (
                  <Switch location={location} status={status} ref={(el) => (this.switch = el)}>
                    {hasAny(ctx.posts)
                      ? ctx.posts.map((post, i) => {
                          return (
                            <Route
                              key={i}
                              exact
                              path={routeUrl(post.link)}
                              render={(props) => (
                                <Post
                                  {...props}
                                  {...post}
                                  index={i}
                                  ref={(el) => (this.Route = el)}
                                />
                              )}
                            />
                          );
                        })
                      : null}

                    {hasAny(ctx.categories)
                      ? ctx.categories.map((category, c) => {
                          return (
                            <Route
                              key={c}
                              exact
                              path={routeUrl(category.link)}
                              render={(props) => (
                                <Category
                                  {...props}
                                  {...category}
                                  // prevLocation={this.state.currentLocation}
                                  ref={(el) => (this.Route = el)}
                                />
                              )}
                            />
                          );
                        })
                      : null}

                    {hasAny(ctx.positions)
                      ? ctx.positions.map((position, i) => {
                          return (
                            <Route
                              key={i}
                              exact
                              path={routeUrl(position.link)}
                              render={(props) => (
                                <Position
                                  {...props}
                                  {...position}
                                  // prevLocation={this.state.currentLocation}
                                  ref={(el) => (this.Route = el)}
                                />
                              )}
                            />
                          );
                        })
                      : null}

                    {hasAny(ctx.members)
                      ? ctx.members.map((member, i) => {
                          return (
                            <Route
                              key={i}
                              exact
                              path={routeUrl(member.link)}
                              render={(props) => (
                                <Member
                                  {...props}
                                  {...member}
                                  // prevLocation={this.state.currentLocation}
                                  ref={(el) => (this.Route = el)}
                                />
                              )}
                            />
                          );
                        })
                      : null}

                    {hasAny(ctx.cases)
                      ? ctx.cases.map((c, i) => {
                          return (
                            <Route
                              key={i}
                              exact
                              path={routeUrl(c.link)}
                              render={(props) => (
                                <Case
                                  {...props}
                                  {...c}
                                  index={i}
                                  // prevLocation={this.state.currentLocation}
                                  ref={(el) => (this.Route = el)}
                                />
                              )}
                            />
                          );
                        })
                      : null}

                    {ctx.services && hasAny(ctx.services)
                      ? ctx.services.map((service, i) => {
                          service['acf']['sections'] = [{ content: service.content.rendered }];
                          const acf = {
                            ...service.acf,
                            sections: [{ content: service.content.rendered }],
                          };
                          return (
                            <Route
                              key={i}
                              exact
                              path={routeUrl(service.link)}
                              render={(props) => (
                                <Page
                                  {...props}
                                  acf={acf}
                                  mod="service"
                                  title={service.title}
                                  ref={(el) => (this.Route = el)}
                                />
                              )}
                            />
                          );
                        })
                      : null}

                    {hasAny(ctx.pages)
                      ? ctx.pages.map((page, i) => {
                          if (page.template === 'templates/agency.php') {
                            const path = ctx.lang === 'lt' ? '/' : `/${ctx.lang}/`;
                            return (
                              <Route
                                ref={(el) => (this.page = el)}
                                key={i}
                                exact
                                {...page}
                                path={path}
                                render={(props) => (
                                  <Agency
                                    {...props}
                                    {...page}
                                    // prevLocation={this.state.currentLocation}
                                    ref={(el) => (this.Route = el)}
                                  />
                                )}
                              />
                            );
                          } else if (page.template === 'templates/contact.php') {
                            return (
                              <Route
                                key={i}
                                exact
                                path={routeUrl(page.link)}
                                render={(props) => (
                                  <Contact
                                    {...props}
                                    {...page}
                                    // prevLocation={this.state.currentLocation}
                                    ref={(el) => (this.Route = el)}
                                  />
                                )}
                              />
                            );
                          } else if (page.template === 'templates/search.php') {
                            return (
                              <Route
                                key={i}
                                exact
                                path={routeUrl(page.link)}
                                render={(props) => (
                                  <Search
                                    {...props}
                                    {...page}
                                    // prevLocation={this.state.currentLocation}
                                    ref={(el) => (this.Route = el)}
                                  />
                                )}
                              />
                            );
                          } else if (page.template === 'templates/blog.php') {
                            // const path = ctx.lang === 'lt' ? '/' : `/${ctx.lang}/`;
                            return (
                              <Route
                                // path={path}
                                path={routeUrl(page.link)}
                                key={i}
                                exact
                                render={(props) => (
                                  <Blog
                                    {...props}
                                    {...page}
                                    posts={ctx.posts}
                                    ref={(el) => (this.Route = el)}
                                  />
                                )}
                              />
                            );
                          } else if (page.template === 'templates/cases.php') {
                            return (
                              <Route
                                key={i}
                                exact
                                path={routeUrl(page.link)}
                                render={(props) => (
                                  <Cases
                                    {...props}
                                    {...page}
                                    cases={ctx.cases}
                                    // prevLocation={this.state.currentLocation}
                                    ref={(el) => (this.Route = el)}
                                  />
                                )}
                              />
                            );
                          } else if (page.template === 'templates/newsletter.php') {
                            return (
                              <Route
                                key={i}
                                exact
                                path={routeUrl(page.link)}
                                render={(props) => (
                                  <Newsletter
                                    {...props}
                                    page={page}
                                    // prevLocation={this.state.currentLocation}
                                    ref={(el) => (this.Route = el)}
                                  />
                                )}
                              />
                            );
                          } else if (page.template === 'templates/career.php') {
                            return (
                              <Route
                                key={i}
                                exact
                                path={routeUrl(page.link)}
                                render={(props) => (
                                  <Career
                                    {...props}
                                    {...page}
                                    // prevLocation={this.state.currentLocation}
                                    ref={(el) => (this.Route = el)}
                                  />
                                )}
                              />
                            );
                          } else {
                            return (
                              <Route
                                key={i}
                                exact
                                path={routeUrl(page.link)}
                                render={(props) => (
                                  <Page
                                    mod="basic"
                                    {...props}
                                    {...page}
                                    // prevLocation={this.state.currentLocation}
                                    ref={(el) => (this.Route = el)}
                                  />
                                )}
                              />
                            );
                          }
                        })
                      : null}

                    {ctx.options && ctx.options.notfound && (
                      <Route
                        render={(props) => (
                          <NotFound
                            {...ctx.options.notfound}
                            {...props}
                            // prevLocation={this.state.currentLocation}
                            ref={(el) => (this.Route = el)}
                          />
                        )}
                      />
                    )}
                  </Switch>
                )}
              </Transition>
            </TransitionGroup>
          )}
        />
      </>
    );
  }
}

AppRouter.contextType = AppContext;

export default AppRouter;
