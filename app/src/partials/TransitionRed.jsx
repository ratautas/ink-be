import React from 'react';
import { Transition } from 'react-transition-group';

import { AppContext } from '../AppContext.jsx';

import { TweenLite, CSSPlugin, Power2 } from 'gsap/all';

// prevent tree shaking
export { CSSPlugin };

class TransitionRed extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onRedEnter = this.onRedEnter.bind(this);
  }

  t(t) {
    return (t * this.context.tranSpeed) / 1000;
  }

  onRedEnter($red) {
    TweenLite.set($red, { right: '0%' });
    TweenLite.fromTo(
      $red,
      // this.t(2.5),
      this.t(5),
      { x: '100%' },
      {
        x: '-130%',
        ease: Power2.easeOut,
        onComplete: () => {},
      },
    );
  }

  render() {
    return (
      <Transition
        in={this.context.pageTransition}
        mountOnEnter={true}
        unmountOnExit={true}
        onEnter={this.onRedEnter}
        // timeout={this.t(1000)}>
        timeout={this.t(2000)}>
        <i
          className="app__transition app__transition--red"
          ref={(el) => (this.$redTransition = el)}
        />
      </Transition>
    );
  }
}

TransitionRed.contextType = AppContext;

export default TransitionRed;
