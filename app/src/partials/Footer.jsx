import React from 'react';
import { NavLink } from 'react-router-dom';

// import { AppConsumer } from '../AppContext.jsx';
import { AppContext } from '../AppContext';

import Socials from '../components/Socials';

import modClass from '../utils/modClass';
import routeUrl from '../utils/routeUrl';
import hasAny from '../utils/hasAny';

class Footer extends React.Component {
  render() {
    const ctx = this.context;
    const footerOptions =
      hasAny(ctx.options) && hasAny(ctx.options.footer) ? ctx.options.footer : null;
    const privacyNavLink =
      hasAny(footerOptions) && hasAny(footerOptions.copyright)
        ? routeUrl(footerOptions.copyright.page)
        : '';
    const footerCopyright =
      hasAny(footerOptions) && hasAny(footerOptions.copyright) ? footerOptions.copyright : null;
    const footer = ctx.options ? ctx.options.footer : null;
    if (ctx.options && ctx.options.footer) {
      return (
        <footer
          className={modClass('footer', this.props.modifier, this.props.className)}
          ref={($el) => (this.$footer = $el)}>
          {hasAny(footerOptions) && (
            <div className="footer__content">
              {hasAny(footerOptions.contacts) && (
                <div className="footer__contacts">
                  {footer.contacts.map((contact, c) => {
                    return contact.link ? (
                      <a href={contact.link} className="footer__contact" key={c}>
                        {contact.label}
                      </a>
                    ) : (
                      <div className="footer__contact" key={c}>
                        {contact.label}
                      </div>
                    );
                  })}
                </div>
              )}
              {hasAny(footerCopyright) && (
                <div className="footer__copyright">
                  {footerCopyright.label}{' '}
                  <NavLink to={privacyNavLink}>{footerCopyright.custom_page_title}</NavLink>
                </div>
              )}
            </div>
          )}
          <Socials className="footer__socials" modifier="footer" />
        </footer>
      );
    } else {
      return null;
    }
  }
}

Footer.contextType = AppContext;

export default Footer;
