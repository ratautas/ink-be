import React from 'react';

const Book = (props) => {
  if (props.book) {
    return (
      <div className={`${props.className ? props.className + ' ' : ''}book`}>
        <div className="book__container container">
          <div className="book__content">
            <i className="book__icon" style={{ backgroundImage: `url('${props.book.icon}')` }} />
            <div className="book__title">
              <h2>{props.book.title}</h2>
            </div>
            <div className="book__text _wysiwyg" dangerouslySetInnerHTML={{ __html: props.book.text }} />
            <a
              className="book__more btn"
              href={props.book.more_link}
              rel="noopener noreferrer"
              target="_blank">
              <span className="btn__text">{props.book.more_label}</span>
              <i className="btn__arrow arrow" />
            </a>
          </div>
        </div>
      </div>
    );
  }
};

export default Book;
