import React from 'react';
import { NavLink } from 'react-router-dom';

import VisibilitySensor from 'react-visibility-sensor';
import { TweenLite, CSSPlugin, Power2 } from 'gsap/all';
import { CustomEase } from '../vendor/gsap/CustomEase';

import Picture from '../elements/Picture';

import { ReactComponent as IconNavLinkedin } from '../assets/img/icon_linkedin.svg';
import { ReactComponent as IconArticles } from '../assets/img/icon_articles.svg';

import chunk from '../utils/chunk';
import routeUrl from '../utils/routeUrl';
// import bg from '../utils/bg';
import unStyle from '../utils/unStyle';
import modClass from '../utils/modClass';

// prevent tree shaking
export { CSSPlugin };

class Members extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      rowLimit: Number(props.limit) / 2 || 2,
    };
  }

  render() {
    let titleVisible = false;
    const chunkedMembers = chunk(this.props.list, 2);

    const status = this.props.list
      ? this.props.list.map(() => {
          return { visible: false };
        })
      : [];

    const handleTitleChange = isVisible => {
      if (!titleVisible) {
        if (isVisible) {
          titleVisible = true;
          TweenLite.fromTo(
            this.$title,
            0.8,
            { y: 50 },
            {
              y: 0,
              ease: CustomEase.create('custom', '.48,0,.12,1'),
              onComplete: () => unStyle(this.$title),
            },
          );
        } else {
          TweenLite.set(this.$title, { y: 50 });
        }
      }
    };

    const handleMemberChange = (isVisible, index) => {
      const $el = this['$member' + index];
      const w = window.innerWidth;
      const x = index % 2 ? -w * 0.2 : w * 0.2;
      const delay = index % 2 ? 0.1 : 0;
      if (!status[index].visible) {
        if (isVisible) {
          status[index].visible = true;
          TweenLite.fromTo(
            $el,
            1,
            { x },
            {
              delay,
              x: 0,
              ease: Power2.easeOut,
              onComplete: () => unStyle($el),
            },
          );
        } else {
          TweenLite.set($el, { x });
        }
      }
    };

    return (
      <div className={modClass('members', this.props.modifier, this.props.className)}>
        {this.props.title && (
          <VisibilitySensor onChange={handleTitleChange} partialVisibility={{ top: 0 }}>
            <div className="members__title" ref={$el => (this.$title = $el)}>
              <h2>{this.props.title}</h2>
            </div>
          </VisibilitySensor>
        )}
        {chunkedMembers.length > 0 && (
          <div className="members__list">
            {chunkedMembers
              .filter((member, row) => row < this.state.rowLimit)
              .map((membersRow, r) => {
                return (
                  <div className="members__row" key={r}>
                    {membersRow.map((id, m) => {
                      const i = (r % 2) * 2 + ((m % 2) + 1);
                      const member = this.props.ctx.membersById[id.member];
                      const index = r * 2 + m;
                      return (
                        member && (
                          <VisibilitySensor
                            key={m}
                            onChange={change => handleMemberChange(change, index)}
                            partialVisibility={{ top: 0 }}>
                            <div
                              className={`members__item members__item--${i}${
                                index === 0 ? ' members__item--first' : ''
                              }`}
                              ref={$el => (this['$member' + index] = $el)}>
                              <div className="members__content">
                                {member.acf && member.acf.image && (
                                  <Picture
                                    url={member.acf.image}
                                    classNames="members__image"
                                    alt={member.title.rendered}
                                    size={308}
                                  />
                                )}
                                {member.title && (
                                  <div className="members__name">{member.title.rendered}</div>
                                )}
                                {member.acf && member.acf.position && (
                                  <div className="members__position">{member.acf.position}</div>
                                )}
                                {member.acf && member.acf.email && (
                                  <a
                                    href={`mailto:${member.acf.email}`}
                                    className={`members__email members__email--${i}`}>
                                    {member.acf.email}
                                  </a>
                                )}
                                {member.content && (
                                  <div
                                    className="members__text _wysiwyg"
                                    dangerouslySetInnerHTML={{
                                      __html: member.content.rendered,
                                    }}
                                  />
                                )}
                                <div className="members__actions">
                                  {member.acf && member.acf.linkedin && (
                                    <a
                                      target="_blank"
                                      rel="noopener noreferrer"
                                      aria-label={member.title.rendered + ' Linkedin'}
                                      href={member.acf.linkedin}
                                      className="members__action members__action--linkedin">
                                      <IconNavLinkedin />
                                    </a>
                                  )}
                                  {member.posts.length > 0 && member.link && (
                                    <NavLink
                                      to={routeUrl(member.link)}
                                      aria-label={member.title.rendered + ' posts'}
                                      className="members__action members__action--articles">
                                      <IconArticles />
                                    </NavLink>
                                  )}
                                </div>
                              </div>
                            </div>
                          </VisibilitySensor>
                        )
                      );
                    })}
                  </div>
                );
              })}
          </div>
        )}
        {Number(this.state.rowLimit) < this.props.list.length && (
          <div className="members__footer">
            <div
              className="members__more more"
              onClick={() => {
                this.setState({ rowLimit: 99 });
              }}>
              <span className="more__label">{this.props.more}</span>
              <i className="more__icon">
                <span className="more__arrow arrow" />
              </i>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Members;
