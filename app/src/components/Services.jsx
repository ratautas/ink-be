import React from 'react';
import { NavLink } from 'react-router-dom';

import { AppContext } from '../AppContext.jsx';

import modClass from '../utils/modClass';
import routeUrl from '../utils/routeUrl';

class Services extends React.PureComponent {
  render() {
    return (
      <div className={modClass('services', this.props.modifier, this.props.className)}>
        <div className="services__title">
          <h2>{this.props.services.title}</h2>
        </div>
        {this.props.services && this.props.services.list.length && (
          <div className="services__list">
            {this.props.services.list.map((serviceItem, s) => {
              var service = this.context.servicesById[serviceItem.service];
              return service ? (
                <NavLink key={s} to={routeUrl(service.link)} className="services__item">
                  {service.title.rendered}
                </NavLink>
              ) : null;
            })}
          </div>
        )}
      </div>
    );
  }
}

Services.contextType = AppContext;

export default Services;
