import React from 'react';
import { NavLink } from 'react-router-dom';
import Formsy from 'formsy-react';

import { AppContext } from '../AppContext.jsx';

import Input from '../elements/Input';
import CheckBox from '../elements/CheckBox';

import routeUrl from '../utils/routeUrl';
import hasAny from '../utils/hasAny';

class Subscription extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmitting: false,
      response: null,
    };

    this.handleValidSubmit = this.handleValidSubmit.bind(this);
  }

  handleValidSubmit = (model) => {
    var data = new FormData();
    for (var key in model) {
      data.append(key, model[key]);
    }
    this.setState({ isSubmitting: true });
    fetch(`${document.body.dataset.root}/wp-json/api/subscribe`, {
      method: 'POST',
      body: data,
    })
      .then((r) => r.json())
      .then((response) => {
        // console.log('Response:');
        // console.log(response);
        this.setState({ response, isSubmitting: false });
      })
      .catch((error) => {
        console.log('Error:');
        console.log(error);
        this.setState({ response: error, isSubmitting: false });
      });
  };

  render() {
    const ctx = this.context;
    const formOptions = hasAny(ctx.options) && hasAny(ctx.options.form) ? ctx.options.form : null;
    const footerOptions =
      hasAny(ctx.options) && hasAny(ctx.options.footer) ? ctx.options.footer : null;
    const privacyOptions =
      hasAny(formOptions) && hasAny(formOptions.privacy) ? formOptions.privacy : null;
    const subscription =
      hasAny(ctx.options) && hasAny(ctx.options.subscription) ? ctx.options.subscription : null;
    const title = !this.props.title && subscription ? subscription.title : this.props.title;
    const text = !this.props.text && subscription ? subscription.text : this.props.text;

    const successText = subscription ? ctx.options.subscription.success : '';

    return (
      <div
        className={`${this.props.className} subscription${
          this.props.modifier ? ' subscription--' + this.props.modifier : ''
        }`}>
        {hasAny(footerOptions) && (
          <div className="subscription__container container container--xs">
            {(hasAny(title) || hasAny(text)) && (
              <div className="subscription__content">
                <div className="subscription__title">
                  <h2>{title}</h2>
                </div>
                <div
                  className="subscription__text _wysiwyg"
                  dangerouslySetInnerHTML={{ __html: text }}
                />
              </div>
            )}
            {!this.state.response ||
            (hasAny(this.state.response) && !hasAny(this.state.response.id)) ? (
              <Formsy className="subscription__form" onValidSubmit={this.handleValidSubmit}>
                {formOptions.email && (
                  <div className="form__row">
                    <Input
                      name="email"
                      label={formOptions.email}
                      validations={{
                        isEmail: true,
                      }}
                      validationErrors={{
                        isEmail: formOptions.email_error,
                      }}
                      modifier={this.props.modifier}
                      required
                    />
                  </div>
                )}
                <div className="form__row">
                  {hasAny(privacyOptions) && (
                    <CheckBox
                      name="agree"
                      required
                      modifier={this.props.modifier}
                      validationErrors={{
                        isDefaultRequiredValue: formOptions.required_error,
                      }}>
                      {privacyOptions.prefix}{' '}
                      <NavLink to={routeUrl(privacyOptions.page)}>
                        <b>{privacyOptions.label}</b>
                      </NavLink>{' '}
                      {privacyOptions.suffix}.
                    </CheckBox>
                  )}
                </div>
                {!this.state.isSubmitting && (
                  <button className="subscription__submit" aria-label="Submit">
                    <i className="subscription__arrow arrow" />
                  </button>
                )}
                {hasAny(this.state.response) && !hasAny(this.state.response.id) && (
                  <div className="form__response">{this.state.response.detail}</div>
                )}
              </Formsy>
            ) : (
              <div className="subscription__success">{successText}</div>
            )}
          </div>
        )}
      </div>
    );
  }
}

Subscription.contextType = AppContext;

export default Subscription;
