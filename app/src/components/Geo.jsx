import React from 'react';

import VisibilitySensor from 'react-visibility-sensor';

import { TweenLite, CSSPlugin, Power2 } from 'gsap/all';

import unStyle from '../utils/unStyle';
import modClass from '../utils/modClass';

// prevent tree shaking
export { CSSPlugin };

export default class Geo extends React.PureComponent {
  render() {
    let titleVisible = false;
    let mapVisible = false;

    const geo_image =
      window.innerWidth < 768 ? this.props.geo_mobile_image : this.props.geo_desktop_image;

    const handleTitleChange = (isVisible, index) => {
      if (!titleVisible) {
        if (isVisible) {
          titleVisible = true;
          TweenLite.fromTo(
            this.$title,
            0.8,
            { y: 50 },
            {
              y: 0,
              ease: Power2.easeOut,
              onComplete: () => unStyle(this.$title),
            },
          );
        } else {
          TweenLite.set(this.$title, { y: 50 });
        }
      }
    };

    const handleMapChange = (isVisible, index) => {
      if (!mapVisible && this.$map) {
        if (isVisible) {
          mapVisible = true;
          TweenLite.fromTo(
            this.$map,
            0.8,
            { y: 50 },
            {
              y: 0,
              ease: Power2.easeOut,
              onComplete: () => unStyle(this.$map),
            },
          );
        } else {
          TweenLite.set(this.$map, { y: 50 });
        }
      }
    };

    return (
      <div className={modClass('geo', this.props.modifier, this.props.className)}>
        {this.props.geo_title && (
          <VisibilitySensor onChange={handleTitleChange} partialVisibility={{ top: 0 }}>
            <div className="geo__title" ref={($el) => (this.$title = $el)}>
              <h2>{this.props.geo_title}</h2>
            </div>
          </VisibilitySensor>
        )}
        {geo_image && (
          <VisibilitySensor onChange={handleMapChange} partialVisibility={{ top: 0 }}>
            <div className="geo__map" ref={($el) => (this.$map = $el)}>
              <img className="geo__image" alt={this.props.geo_title} src={geo_image} />
            </div>
          </VisibilitySensor>
        )}
      </div>
    );
  }
}
