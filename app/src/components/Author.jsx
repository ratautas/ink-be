import React from 'react';
import { NavLink } from 'react-router-dom';

import { AppConsumer } from '../AppContext.jsx';

import Picture from '../elements/Picture';

import modClass from '../utils/modClass';
import bg from '../utils/bg';
import routeUrl from '../utils/routeUrl';
import hasAny from '../utils/hasAny';

const Author = props => {
  // console.log(props.author.acf);
  return (
    <AppConsumer>
      {ctx => {
        const miscOptions =
          hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;
        if (props.author) {
          return (
            <div className={modClass('author', props.modifier, props.className)}>
              {hasAny(props.author.acf) && props.author.acf.image && (
                <div className={modClass('author__media', props.modifier)}>
                  <Picture
                    url={props.author.acf.image}
                    classNames="media__image"
                    alt={miscOptions.author}
                    size={96}
                  />
                </div>
              )}
              <div className={modClass('author__info', props.modifier)}>
                {miscOptions && miscOptions.author && props.label && (
                  <span className={modClass('author__label', props.modifier)}>
                    {miscOptions.author}:
                  </span>
                )}
                {props.author.title && (
                  <div className={modClass('author__name', props.modifier)}>
                    {props.author.title.rendered}
                  </div>
                )}
              </div>
              {props.author.title && props.author.link && (
                <NavLink to={routeUrl(props.author.link)} className="author__link">
                  {props.author.title.rendered}
                </NavLink>
              )}
            </div>
          );
        } else {
          return null;
        }
      }}
    </AppConsumer>
  );
};

export default Author;
