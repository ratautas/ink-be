import React from 'react';
import { NavLink } from 'react-router-dom';

import { AppConsumer } from '../AppContext.jsx';
import Socials from '../components/Socials';

import bg from '../utils/bg';
import hasAny from '../utils/hasAny';
import routeUrl from '../utils/routeUrl';

const Bio = (props) => {
  return (
    <AppConsumer>
      {(ctx) => {
        const miscOptions =
          hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;
        return (
          <div className={`${props.className ? props.className + ' ' : ''}bio`}>
            <div className="bio__container container">
              <div className="bio__wrapper">
                {hasAny(props.acf) && props.acf.image && (
                  <div className="bio__media media">
                    <i className="media__image" style={bg(props.acf.image)} />
                  </div>
                )}
                <div className="bio__content">
                  {miscOptions && miscOptions.author && (
                    <div className="bio__label">{miscOptions.author}</div>
                  )}
                  {hasAny(props.title) && (
                    <NavLink to={routeUrl(props.link)} className="bio__name trigger">
                      {props.title.rendered}
                    </NavLink>
                  )}
                  {hasAny(props.content) && (
                    <div
                      className="bio__text _wysiwyg"
                      dangerouslySetInnerHTML={{ __html: props.content.rendered }}
                    />
                  )}
                </div>
              </div>
            </div>
            <Socials className="bio__socials" modifier="bio" />
          </div>
        );
      }}
    </AppConsumer>
  );
};

export default Bio;
