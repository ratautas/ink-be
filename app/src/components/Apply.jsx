import React from 'react';
import Formsy from 'formsy-react';
import { NavLink } from 'react-router-dom';

import { AppContext } from '../AppContext.jsx';

import Submit from '../elements/Submit';
import Input from '../elements/Input';
import File from '../elements/File';
import TextArea from '../elements/TextArea';
import CheckBox from '../elements/CheckBox';

import routeUrl from '../utils/routeUrl';
import hasAny from '../utils/hasAny';
import modClass from '../utils/modClass';

class Apply extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      formStage: 0,
      duration: 500,
      response: null,
    };

    this.handleValidSubmit = this.handleValidSubmit.bind(this);
    this.handleInvalidSubmit = this.handleInvalidSubmit.bind(this);
  }

  handleInvalidSubmit = () => {
    setTimeout(() => {
      const $errInputs = document.querySelectorAll(
        'input.has-error, textarea.has-error, select.has-error',
      );
      if ($errInputs.length) {
        $errInputs[0].focus();
      }
    }, 0);
  };

  handleValidSubmit = (model) => {
    this.setState({ formStage: 1 });
    const position = this.props.position || '';
    let data = new FormData();
    for (var key in model) data.append(key, model[key]);
    data.append('position', position);
    fetch(`${document.body.dataset.root}/wp-json/api/apply`, {
      method: 'POST',
      body: data,
    })
      .then((r) => r.json())
      .then((response) => {
        // console.log(response);
        if (response.success === 1) {
          this.setState({ formStage: 2 });
          setTimeout(() => {
            this.setState({ response, formStage: 3 });
          }, this.state.duration);
        } else if (response.success === 0) {
          // console.log('Error:' + response.title);
          this.setState({ formStage: 2 });
          setTimeout(() => {
            this.setState({ response, formStage: 4 });
          }, this.state.duration);
        }
      })
      .catch((error) => {
        // console.log('Error:' + error);
        this.setState({ formStage: 2 });
        setTimeout(() => {
          this.setState({ response: error, formStage: 4 });
        }, this.state.duration);
      });
  };

  render() {
    const ctx = this.context;
    const formOptions = hasAny(ctx.options) && hasAny(ctx.options.form) ? ctx.options.form : null;
    const privacyOptions =
      hasAny(formOptions) && hasAny(formOptions.privacy) ? formOptions.privacy : null;

    const miscOptions = hasAny(ctx.options) && hasAny(ctx.options.misc) ? ctx.options.misc : null;
    return (
      <div className={modClass('apply', this.props.modifier, this.props.className)}>
        {this.props.heading && hasAny(miscOptions) && miscOptions.apply && (
          <div className="apply__top">
            <h2>{miscOptions.apply}</h2>
          </div>
        )}
        <div className="apply__content">
          {this.props.lead && (
            <div
              className="apply__lead _wysiwyg"
              dangerouslySetInnerHTML={{ __html: this.props.lead }}
            />
          )}
          {this.props.lead && (
            <div
              className="apply__text _wysiwyg"
              dangerouslySetInnerHTML={{ __html: this.props.text }}
            />
          )}
          {hasAny(formOptions) && (
            <Formsy
              className="apply__form"
              onValidSubmit={this.handleValidSubmit}
              onInvalidSubmit={this.handleInvalidSubmit}>
              {formOptions.name && (
                <div className="form__row">
                  <Input
                    name="name"
                    label={formOptions.name}
                    validations={{
                      minLength: 3,
                    }}
                    validationErrors={{
                      isDefaultRequiredValue: formOptions.required_error,
                      minLength: formOptions.name_error,
                    }}
                    required
                  />
                </div>
              )}
              {formOptions.email && (
                <div className="form__row">
                  <Input
                    name="email"
                    label={formOptions.email}
                    validations={{
                      isEmail: true,
                    }}
                    validationErrors={{
                      isEmail: formOptions.email_error,
                      isDefaultRequiredValue: formOptions.required_error,
                    }}
                    required
                  />
                </div>
              )}
              {formOptions.tel && (
                <div className="form__row">
                  <Input
                    name="tel"
                    label={formOptions.tel}
                    validations={{
                      matchRegexp: /[()+0-9\s]+$/,
                    }}
                    validationErrors={{
                      matchRegexp: formOptions.phone_error,
                    }}
                  />
                </div>
              )}
              {formOptions.cv && (
                <div className="form__row">
                  <File
                    name="cv"
                    label={formOptions.cv}
                    accept=".png,.jpg,.jpeg,.pdf,.doc,.docx"
                    type="file"
                    validationErrors={{
                      isDefaultRequiredValue: formOptions.required_error,
                    }}
                    required
                  />
                </div>
              )}
              {formOptions.message && (
                <div className="form__row">
                  <TextArea name="message" label={formOptions.message} rows="1" />
                </div>
              )}
              {privacyOptions && (
                <div className="form__row form__row--wrap">
                  <CheckBox
                    name="agree"
                    modifier="apply"
                    required
                    validationErrors={{
                      isDefaultRequiredValue: formOptions.required_error,
                    }}>
                    {privacyOptions.prefix}{' '}
                    <NavLink to={routeUrl(privacyOptions.page)}>
                      <b>{privacyOptions.label}</b>
                    </NavLink>{' '}
                    {privacyOptions.suffix}.
                  </CheckBox>
                  {hasAny(formOptions) && hasAny(formOptions.send) && (
                    <Submit
                      formStage={this.state.formStage}
                      duration={this.state.duration}
                      className="apply__submit"
                    />
                  )}
                </div>
              )}
              {hasAny(this.state.response) && Number(this.state.response.success) === 0 && (
                <div className="form__response">{this.state.response.detail}</div>
              )}
            </Formsy>
          )}
        </div>
      </div>
    );
  }
}

Apply.contextType = AppContext;

export default Apply;
