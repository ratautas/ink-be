import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import VisibilitySensor from 'react-visibility-sensor';

import Swiper from 'react-id-swiper';
import { TweenLite, CSSPlugin, Power2 } from 'gsap/all';

import Picture from '../elements/Picture';

import routeUrl from '../utils/routeUrl';
import unStyle from '../utils/unStyle';
import modClass from '../utils/modClass';
import chunk from '../utils/chunk';

// prevent tree shaking
export { CSSPlugin };

export default class Clients extends React.PureComponent {
  render() {
    let titleVisible = false;

    // const status = this.props.list
    //   ? this.props.list.map((item, index) => {
    //       return { visible: false };
    //     })
    //   : [];

    const chunkedClients = chunk(this.props.list, 12);

    const handleTitleChange = (isVisible, index) => {
      if (!titleVisible) {
        if (isVisible) {
          titleVisible = true;
          TweenLite.fromTo(
            this.$title,
            0.8,
            { y: 50 },
            {
              y: 0,
              ease: Power2.easeOut,
              onComplete: () => unStyle(this.$title),
            },
          );
        } else {
          TweenLite.set(this.$title, { y: 50 });
        }
      }
    };

    const swiperParams = {
      speed: 800,
      slidesPerView: 1,
      loop: true,
      navigation: {
        nextEl: '.clients__next.arrow',
        prevEl: '.clients__prev.arrow',
      },
    };

    return (
      <div className={modClass('clients', this.props.modifier, this.props.className)}>
        {this.props.title && (
          <VisibilitySensor onChange={handleTitleChange} partialVisibility={{ top: 0 }}>
            <div className="clients__title" ref={$el => (this.$title = $el)}>
              <h2>{this.props.title}</h2>
            </div>
          </VisibilitySensor>
        )}
        <div className="clients__slider">
          <Swiper {...swiperParams}>
            {chunkedClients.map((list, l) => {
              return (
                <div className="clients__slide" key={l}>
                  <div className="clients__list">
                    {list.map((client, c) => {
                      return (
                        <Fragment key={c}>
                          {client.link ? (
                            <a
                              key={c}
                              target="_blank"
                              rel="noopener noreferrer"
                              href={client.link}
                              ref={$el => (this['$client' + c] = $el)}
                              className="clients__item clients__item--link">
                              {client.logo && (
                                <Picture
                                  url={client.logo}
                                  classNames="clients__logo clients__logo--link"
                                  alt={client.link}
                                  size={500}
                                />
                              )}
                            </a>
                          ) : (
                            <span
                              key={c}
                              className="clients__item"
                              ref={$el => (this['$client' + c] = $el)}>
                              {client.logo && (
                                <img
                                  src={client.logo}
                                  alt={client.logo}
                                  className="clients__logo"
                                />
                              )}
                            </span>
                          )}
                        </Fragment>
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </Swiper>
        </div>
        {this.props.cases_page && this.props.cases_label && (
          <div className="clients__footer">
            <NavLink to={routeUrl(this.props.cases_page)} className="clients__cases">
              {this.props.cases_label}
              <i className="clients__arrow arrow" />
            </NavLink>
          </div>
        )}
      </div>
    );
  }
}
