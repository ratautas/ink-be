import React from 'react';

import { AppConsumer } from '../AppContext.jsx';

import modClass from '../utils/modClass';
import hasAny from '../utils/hasAny';

const Socials = (props) => {
  return (
    <AppConsumer>
      {(ctx) => {
        const footer =
          hasAny(ctx.options) && hasAny(ctx.options.footer) ? ctx.options.footer : null;
        const socials = hasAny(footer) && hasAny(footer.socials) ? footer.socials : null;

        if (ctx.ready && socials) {
          return (
            <div className={modClass('socials', props.modifier, props.className)}>
              <ul className={modClass('socials__list', props.modifier)}>
                {ctx.options.footer.socials.map((social, s) => {
                  return (
                    <li className="socials__item" key={s}>
                      <a
                        href={social.url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="socials__trigger"
                        style={{ backgroundImage: `url('${social.icon}')` }}>
                        {social.url}
                      </a>
                    </li>
                  );
                })}
              </ul>
            </div>
          );
          // } else {
          //   return null;
        }
      }}
    </AppConsumer>
  );
};

export default Socials;
