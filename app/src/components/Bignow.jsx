import React from 'react';

import bg from '../utils/bg';
import modClass from '../utils/modClass';

function Bignow(props) {
  return (
    <div className={modClass('bignow', props.modifier, props.className)}>
      {props.logo && <div className="bignow__logo" style={bg(props.logo)} />}
      {props.intro && (
        <div className="bignow__intro _wysiwyg" dangerouslySetInnerHTML={{ __html: props.intro }} />
      )}
      {props.text && (
        <div className="bignow__text _wysiwyg" dangerouslySetInnerHTML={{ __html: props.text }} />
      )}
      {props.more_link && props.more_label && (
        <div className="bignow__more">
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={props.more_link}
            className="bignow__link">
            {props.more_label}
            <i className="bignow__arrow arrow" />
          </a>
        </div>
      )}
    </div>
  );
}

export default Bignow;
