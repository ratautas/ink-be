import React from 'react';
import { Transition } from 'react-transition-group';

import { TweenLite, CSSPlugin, Power2 } from 'gsap/all';

import { AppContext } from '../AppContext.jsx';

import unStyle from '../utils/unStyle';

// prevent tree shaking
export { CSSPlugin };

class Modal extends React.PureComponent {
  constructor(props) {
    super(props);

    this.handleKeydown = this.handleKeydown.bind(this);
    this.handleKeydown = this.handleKeydown.bind(this);
  }

  onModalEnter($el) {
    TweenLite.fromTo(
      $el,
      0.6,
      { opacity: 0, scale: 1.2 },
      {
        opacity: 1,
        scale: 1,
        ease: Power2.easeOut,
        onComplete: () => {
          $el.focus();
          unStyle($el);
        },
      },
    );
  }

  onModalExit($el) {
    TweenLite.to($el, 0.6, {
      opacity: 0,
      scale: 1.2,
      ease: Power2.easeOut,
    });
  }

  handleKeydown(e) {
    if (Number(e.keyCode) === 27) this.context.closeModal();
  }

  render() {
    return (
      <Transition
        in={this.context.activeModal === this.props.title}
        mountOnEnter={true}
        unmountOnExit={true}
        onEnter={this.onModalEnter}
        onExit={this.onModalExit}
        timeout={600}>
        <div
          className="modal"
          onKeyDown={(e) => {
            if (Number(e.keyCode) === 27) this.context.closeModal();
          }}
          ref={($el) => (this.$modal = $el)}
          tabIndex="0">
          <div className="modal__dialog">
            <div className="modal__content">
              <div className="modal__top">
                <h2>{this.props.title}</h2>
              </div>
              {this.props.image && (
                <img className="modal__image" alt={this.props.title} src={this.props.image} />
              )}
              {this.props.text && (
                <div
                  className="modal__text _wysiwyg"
                  dangerouslySetInnerHTML={{ __html: this.props.text }}
                />
              )}
            </div>
          </div>
          <i className="modal__backdrop" onClick={this.context.closeModal} />
          <i className="modal__close" onClick={this.context.closeModal} />
        </div>
      </Transition>
    );
  }
}

Modal.contextType = AppContext;

export default Modal;
