import React from 'react';

import hasAny from '../utils/hasAny.js';

const Publishers = (props) => {
  if (props.publishers) {
    return (
      <div className={`${props.className ? ' ' + props.className : ''}publishers`}>
        <div className="publishers__container container">
          <div className="publishers__content">
            {props.publishers.label && (
              <div className="publishers__label">{props.publishers.label}</div>
            )}
            <div className="publishers__list">
              {hasAny(props.publishers.list) &&
                props.publishers.list.map((publisher, p) => {
                  return (
                    <div className="publishers__item" key={p}>
                      {publisher.logo && (
                        <img src={publisher.logo} alt="" className="publishers__logo" />
                      )}
                      {publisher.url && (
                        <a
                          href={publisher.url}
                          target="_blank"
                          rel="noopener noreferrer"
                          className="publishers__link">
                          {publisher.url}
                        </a>
                      )}
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Publishers;
