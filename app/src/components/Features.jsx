import React from 'react';

import modClass from '../utils/modClass';

export default class Features extends React.PureComponent {
  render() {
    return (
      <div
        className={modClass('features', this.props.modifier, this.props.className)}
        ref={($el) => (this.$features = $el)}>
        <div className="features__list">
          {this.props.features.map((feature, f) => {
            return (
              <div className="features__item" key={f}>
                <i
                  className="features__icon"
                  style={{ backgroundImage: `url('${feature.icon}')` }}
                />
                <div className="features__content">
                  <div className="features__title">{feature.title}</div>
                  <div
                    className="features__text _wysiwyg"
                    dangerouslySetInnerHTML={{ __html: feature.text }}
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
