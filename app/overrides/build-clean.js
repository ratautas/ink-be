//// IMPORTANT - BUILD DOES NOT FAIL.
//// It throws validation error from printFileSizesAfterBuild() function at
//// node_modules/react-scripts/scripts/build.js lines 108-114
//// comment it to get a seamless build

const rewire = require('rewire');
const defaults = rewire('react-scripts/scripts/build.js');
const WebappWebpackPlugin = require('webapp-webpack-plugin');
const config = defaults.__get__('config');
const critical = require('critical');

const path = require('path');
const fs = require('fs');

// Consolidate chunk files instead
config.optimization.splitChunks = {
  cacheGroups: {
    default: false,
  },
};

// Move runtime into bundle instead of separate file
config.optimization.runtimeChunk = false;

// JS
config.output.path = path.resolve(fs.realpathSync(process.cwd()), '../wp-content/themes/api/assets');
config.output.filename = 'js/app.js';

// encode everything to base64
config.module.rules[2].oneOf[0] = {
  test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/, /\.woff$/],
  loader: require.resolve('url-loader'),
  options: {
    limit: 100000,
    name: 'static/[name].[ext]',
  },
}

// config.module.rules[2].oneOf[7].options.name = 'static/[name].[ext]';

// CSS. "5" is MiniCssPlugin
config.plugins[5].options.filename = 'css/app.css';
// add favicon generation
config.plugins.push(
  new WebappWebpackPlugin({
    cache: true,
    prefix: 'img/webapp/',
    logo: './src/assets/img/favicon.png',
    publicPath: '../../',
    favicons: {
      appName: 'INK agency',
      developerName: 'Algirdas Tamasauskas',
      background: '#f1f2f2',
      theme_color: '#ea3824',
      start_url: '/',
    },
  }),
);


// critical.generate({
//   // Inline the generated critical-path CSS
//   // - true generates HTML
//   // - false generates CSS
//   inline: true,

//   // Your base directory
//   base: 'dist/',

//   // HTML source
//   html: '<html>...</html>',

//   // HTML source file
//   src: 'index.html',

//   // Your CSS Files (optional)
//   css: ['dist/styles/main.css'],

//   // Viewport width
//   width: 1300,

//   // Viewport height
//   height: 900,

//   // Output results to file
//   target: {
//     css: 'critical.css',
//     html: 'index-critical.html',
//     uncritical: 'uncritical.css'
//   },

//   // Minify critical-path CSS when inlining
//   minify: true,

//   // Extract inlined styles from referenced stylesheets
//   extract: true,

//   // Complete Timeout for Operation
//   timeout: 30000,

//   // ignore CSS rules
//   ignore: {
//     atrule: ['@font-face'],
//     rule: [/some-regexp/],
//     decl: (node, value) => /big-image\.png/.test(value)
//   }
// });